$(".tab-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "fade"
    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        finish: "Submit"
    }
    , onFinished: function (event, currentIndex) {
       swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
            
    }
});


var form = $(".validation-wizard").show();

$(".validation-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "fade"
    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        finish: "Install"
    }
    , onStepChanging: function (event, currentIndex, newIndex) {
        return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
    }
    , onFinishing: function (event, currentIndex) {
        return form.validate().settings.ignore = ":disabled", form.valid()
    }
    , onFinished: function (event, currentIndex) {
        // var db_type = $('#db_type2').val();
        // var db_name = $('#dbname2').val();
        // var db_user = $('#dbuname2').val();
        // var db_pwd = $('#dbpwd2').val();
        // var db_host = $('#dbhost2').val();
        // var admin_user = $('#adminuname2').val();
        // var admin_email = $("#adminemail2").val();
        // var admin_pwd = $("#adminpwd2").val();
        //
        // $.post('', { db_type: db_type, db_name: db_name, db_user: db_user, db_pwd: db_pwd,
        //     db_host: db_host, admin_user: admin_user, admin_email: admin_email, admin_pwd: admin_pwd }).done(function(data) {
        //     console.log(data);
        //     swal("Congrats!", "Installation complete. Bla bla Bla").then(
        //         function(configResponse) {
        //             // location.reload();
        //             // window.location.href = '/';
        //             // window.location = '/';
        //             // console.error(configResponse);
        //         }
        //     );
        // });

        // todo ffs
        $("#btninstall").click();
    }
}), $(".validation-wizard").validate({
    ignore: "input[type=hidden]"
    , errorClass: "text-danger"
    , successClass: "text-success"
    , highlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , unhighlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , errorPlacement: function (error, element) {
        error.insertAfter(element)
    }
    , rules: {
        email: {
            email: !0
        }
    }
})