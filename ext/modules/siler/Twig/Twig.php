<?php declare(strict_types=1);
/*
 * Helper functions to work with the Twig template engine.
 */

namespace Siler\Twig;

use Siler\Container;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;
use UnexpectedValueException;
use Twig\TwigFunction;
use WebTree\lib\int\Themes\ThemeChecker;
use WebTree\Tangle as tangle;
use WebTree\Formatting as formatting;
use DateTime;
use RuntimeException;
use WebTree\User;

/**
 * Initialize the Twig environment.
 *
 * @param string $templatesPath Path to templates
 * @param string|false $templatesCachePath Path to templates cache
 * @param bool $debug Should TwigEnv allow debugging
 *
 * @return Environment
 */
function init(string $templatesPath, $templatesCachePath = false, bool $debug = false): Environment
{
    $twig = new Environment(
        new FilesystemLoader($templatesPath),
        [
            'debug' => $debug,
            'cache' => $templatesCachePath
        ]
    );
    $twig->addExtension(new \Twig_Extensions_Extension_Date());
    $twig->addExtension(new \Twig_Extensions_Extension_Array());
    $twig->addExtension(new \Twig_Extensions_Extension_Text());
    $twig->addExtension(new \Twig_Extension_Debug());
    $twig->addExtension(new \Twig_Extensions_Extension_I18n());
    $twig->addFunction(new TwigFunction(
    /**
     * @param $function
     * @param array $data
     * @return string|null
     */
        'fn', function ($function, $data = []) {

        if ($function == 'nonceGen') {  //todo premesti u neku internu kuku
            return uniqid(md5((string)mt_rand()));
        }

        if ($function == 'pwdGen') {
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*()_+.';
            $pass = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 13; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
            return implode($pass); //turn the array into a string
        }

        if ($function == 'wt_head') { // todo filteri
            $tangle = new tangle();
            $tangle->hooks->do_action('wt_head');
        }

        if ($function == 'wt_foot') {
            $tangle = new tangle();
            $tangle->hooks->do_action('wt_foot');
        }

        if ($function == 'years_today') {

            $d1 = new DateTime($data[0]);
            $d2 = new DateTime(date('Y-m-d'));

            $diff = $d2->diff($d1);

            return $diff->y;
        }

        if ($function == 'current_user') {
            $user = new user();

            if (isset($userData)) {
                if (isset($user, $user->currentUser)) {
                    $user->currentUser['data'] = $userData;
                }
            }

            return isset($user, $user->currentUser) ? $user->currentUser : null;
        }

        if ($function == 'get_user') {
            $user = new User();
            isset($data, $data[0]) ? $id = $data[0] : $id = null;
            return isset($id) ? $user->getUser((int)$id) : null;
        }

        if ($function == 'is_theme_installed') {
            $checker = new ThemeChecker();
            isset($data, $data[0]) ? $name = $data[0] : $name = null;
            return isset($name) ? $checker->isThemeInstalled((string)$name) : null;
        }

        if ($function == 'get_excerpt') {
            $format = new formatting();
            isset($data, $data[0]) ? $string = $data[0] : $string = null;
            return isset($string) ? $format->getExcerpt($string, 0, 30) : null;
        }

        return null;

    }));
    Container\set('twig', $twig);

    return $twig;
}

/**
 * Renders the given template within the given data.
 *
 * @param string $name The template name in the templates path
 * @param array $data The array of data to used within the template
 *
 * @return string
 *
 * @throws LoaderError
 * @throws RuntimeError
 * @throws SyntaxError
 */
function render(string $name, array $data = []): string
{
    /** @var Environment|null $twig */
    $twig = Container\get('twig');

    if ($twig === null) {
        throw new UnexpectedValueException('Twig should be initialized first');
    }

    return $twig->render($name, $data);
}
