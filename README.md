WebTree
====
```bash
                        ,@@@@@@                             
                 .((((. @@    @# @@@@@@@@@@@@@@             
                 @(  .@ @@%%%%@# @@@        @@@             
                                 @@@        @@@             
                 @@@@@@@@@@@@@@  @@(        @@@             
                 @@@        ,@@  @@@@@@@@@@@@@@ @   @       
         @    (@ @@@        ,@@                 @@@@@       
        *@    (@ @@@        ,@@ @@    @@ @@@@@@@@@@@@@@     
                 @@@@@@@@@@@@@@ @@,,,,@@ @@@        @@@     
      @@@@@@@@@@@@@@                     @@@        @@@     
      @@@        &@@ /@@@@@@@@@@@@@.     @@#        @@@     
      @@@        &@@ %@@         @@.     @@@@@@@@@@@@@@     
      @@@        &@@ @@@         @@. @@@@@@@    @@@         
      @@@@@@@@@@@@@@ @@@*********@@. @     @    @@@         
             ,,,,,,  (@@@@@@@@@@@@@  @@@@@@@    @@@         
            @@    @@   @@*  @@@      @@@    @@@@@@@         
            @@***#@@   @@*  @@@      @@@@@@@@               
                @@#    @@@@@@@@@@@@@@@@@                    
                @@#         @@@                             
                 &@@@@@@  @@@@/                             
                       @@@@                                 
                        @@@                                 
                        @@@                                 
                        @@@  
```
A [WebTree][webtree] is released under the open
source [MIT-license][MIT-license].

It is quick to set up, easy to configure, uses elegant templates, and above
all, it's a joy to use!

WebTree is created using modern open source libraries, and is best suited to build
sites in HTML5 with modern markup.

Installation
------------
clone / get/unzip archive

cd into it

`$ composer install`

as alpha version unstable code is all over, go for composer dump

`$ composer dump -o`

`$ php bin/console` is your friend, use it to install static assets, libraries, themes, etc

to start your local server 
`$ php bin/console server:run`

What's inside
-------------

```
WebTree
│   README.md
│   .ini.php - (dz)ini in PHP    
│   wtc.sh - \containering init
│
└───bin
│   └───console.php - internal CLI
│   
└───core
│   │
│   └───App
│   │
│   └───controllers
│   │
│   └───lib - internal and external libraries
│   │
│   └───views
│
│
└───ext
│
└───static
│   │
│   └───assets
│   │
│   └───lib - static libraries
|
```

Reporting issues
----------------

See our [Contributing to WebTree][contributing] guide.

Support
-------

Have a question? Want to chat? Run into a problem? See our [community][support]
page.

---


[webtree]: https://webtree.iridiumintel.com
[MIT-license]: http://opensource.org/licenses/mit-license.php
[docs]: https://docs.webtree.io/installation
[support]: https://webtree.io/community
[contributing]: https://gitlab.com/webtree/webtree-core/CONTRIBUTING.md