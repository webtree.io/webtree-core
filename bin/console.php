#!/usr/bin/env php
<?php

/** Define ABSPATH as this file's directory */
if ( ! defined( 'THEPATH' ) ) {
    define( 'THEPATH', dirname( __FILE__ ) . '/../' );
}

require_once THEPATH.'core/lib/vendors/autoload.php';

use Symfony\Component\Console\Application;
use WebTree\ConsoleApp\Commands\Assets\AssetsInstall;
use WebTree\ConsoleApp\Commands\Assets\AssetsList;
use WebTree\ConsoleApp\Commands\Cron\CronAdd;
use WebTree\ConsoleApp\Commands\Cron\CronCheck;
use WebTree\ConsoleApp\Commands\Cron\CronRm;
use WebTree\ConsoleApp\Commands\Libs\LibStaticInstall;
use WebTree\ConsoleApp\Commands\Libs\LibStaticList;
use WebTree\ConsoleApp\Commands\Migration\Migration;
use WebTree\ConsoleApp\Commands\Server\ClearCache;
use WebTree\ConsoleApp\Commands\Server\ServerRun;
use WebTree\ConsoleApp\Commands\Server\ServerStop;
use WebTree\ConsoleApp\Commands\Themes\ThemesInstall;
use WebTree\ConsoleApp\Commands\Themes\ThemesList;
use WebTree\ConsoleApp\Commands\User\RegisterAdmin;
use WebTree\ConsoleApp\ConsoleApp;
use WebTree\SecOps as secops;
use RedBeanPHP\Facade as R;


$so = new secops();
$configContainer = $so->get_config(THEPATH.'.webtree.ini.php');

if (!isset($configContainer)) {
    echo 'Config file is missing. Installed?';
    exit(2);
}

// todo defined in install process
// init DB
if (isset($configContainer, $configContainer['database'],
    $configContainer['database']['type'], $configContainer['database']['name'])) {

    $db_type = $configContainer['database']['type'];
    $db_name = $configContainer['database']['name'];
    isset($configContainer['database']['host']) ? $db_host = $configContainer['database']['host'] : $db_host = null;
    isset($configContainer['database']['user']) ? $db_user = $configContainer['database']['user'] : $db_user = null;
    isset($configContainer['database']['pwd']) ? $db_pwd = $configContainer['database']['pwd'] : $db_pwd = null;

    if (strtolower($db_type) == 'sqlite') {
        R::setup('sqlite:'.$db_name.'.db');
    } elseif (strtolower($db_type) == 'mysql' || strtolower($db_type) == 'mariadb') {
        R::setup( 'mysql:host='.$db_host.';dbname='.$db_name,
            $db_user, $db_pwd ); //for both mysql or mariaDB
    } elseif (strtolower($db_type) == 'pgsql') {
        R::setup( 'pgsql:host='.$db_host.';dbname='.$db_name,
            $db_user, $db_pwd );
    } elseif (strtolower($db_type) == 'cubrid') {
        R::setup('cubrid:host='.$db_host.';port=30000;
    dbname='.$db_name,
            $db_user,$db_pwd);
    }
}

$app = new ConsoleApp('WebTree CLI', '0.0.1');
$app -> add(new ServerRun());
$app -> add(new ServerStop());
$app -> add(new RegisterAdmin());
$app -> add(new ClearCache());
$app -> add(new Migration());
$app -> add(new LibStaticInstall());
$app -> add(new LibStaticList());
$app -> add(new AssetsInstall());
$app -> add(new AssetsList());
$app -> add(new ThemesList());
$app -> add(new ThemesInstall());
$app -> add(new CronCheck());
$app -> add(new CronAdd());
$app -> add(new CronRm());
try {
    $app->run();
} catch (Exception $e) {
}