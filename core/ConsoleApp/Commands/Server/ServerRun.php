<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/9/19
 * Time: 2:16 AM
 */

namespace WebTree\ConsoleApp\Commands\Server;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\SecOps;
use WebTree\WebTree as webtree;


class ServerRun extends Command
{
    protected function configure()
    {
        $this->setName('server:run')
            ->setDescription('Runs WebTree Built-in Server')
            ->setHelp('text text')
            ->addArgument('port', InputArgument::OPTIONAL, 'Custom port to run on', '44400');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logo = <<<LOGO
                        ,@@@@@@                             
                 .((((. @@    @# @@@@@@@@@@@@@@             
                 @(  .@ @@%%%%@# @@@        @@@             
                                 @@@        @@@             
                 @@@@@@@@@@@@@@  @@(        @@@             
                 @@@        ,@@  @@@@@@@@@@@@@@ @   @       
         @    (@ @@@        ,@@                 @@@@@       
        *@    (@ @@@        ,@@ @@    @@ @@@@@@@@@@@@@@     
                 @@@@@@@@@@@@@@ @@,,,,@@ @@@        @@@     
      @@@@@@@@@@@@@@                     @@@        @@@     
      @@@        &@@ /@@@@@@@@@@@@@.     @@#        @@@     
      @@@        &@@ %@@         @@.     @@@@@@@@@@@@@@     
      @@@        &@@ @@@         @@. @@@@@@@    @@@         
      @@@@@@@@@@@@@@ @@@*********@@. @     @    @@@         
             ,,,,,,  (@@@@@@@@@@@@@  @@@@@@@    @@@         
            @@    @@   @@*  @@@      @@@    @@@@@@@         
            @@***#@@   @@*  @@@      @@@@@@@@               
                @@#    @@@@@@@@@@@@@@@@@                    
                @@#         @@@                             
                 &@@@@@@  @@@@/                             
                       @@@@                                 
                        @@@                                 
                        @@@                                 
                        @@@            
 
LOGO;
        $wt = new webtree();
        $_port = $input->getArgument('port');
        $output->writeln('<info>'.$logo.'</info>');
        if (!$wt->psRegex('php -S 0.0.0.0')) {
            $output->writeln('<info>Starting Server on http://0.0.0.0:'.$_port.'</info>');
            $output->writeln('<comment>Use "server:stop" command to stop it</comment>');
            exec("php -S 0.0.0.0:".$_port." -t . > log/webtree-server-log".date('m-d-Y_hia')." 2>&1 &");
        } else {
            $output->writeln('<comment>WebTree server instance already running</comment>');
        }


    }
}