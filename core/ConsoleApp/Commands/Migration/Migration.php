<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/9/19
 * Time: 2:16 AM
 */

namespace WebTree\ConsoleApp\Commands\Migration;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\SecOps;
use WebTree\WebTree as webtree;
use RedBeanPHP\Facade as R;


class Migration extends Command
{
    protected function configure()
    {
        $this->setName('migrate:out')
            ->setDescription('Creates sql migration file')
            ->setHelp('text text');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logo = <<<LOGO
                        ,@@@@@@                             
                 .((((. @@    @# @@@@@@@@@@@@@@             
                 @(  .@ @@%%%%@# @@@        @@@             
                                 @@@        @@@             
                 @@@@@@@@@@@@@@  @@(        @@@             
                 @@@        ,@@  @@@@@@@@@@@@@@ @   @       
         @    (@ @@@        ,@@                 @@@@@       
        *@    (@ @@@        ,@@ @@    @@ @@@@@@@@@@@@@@     
                 @@@@@@@@@@@@@@ @@,,,,@@ @@@        @@@     
      @@@@@@@@@@@@@@                     @@@        @@@     
      @@@        &@@ /@@@@@@@@@@@@@.     @@#        @@@     
      @@@        &@@ %@@         @@.     @@@@@@@@@@@@@@     
      @@@        &@@ @@@         @@. @@@@@@@    @@@         
      @@@@@@@@@@@@@@ @@@*********@@. @     @    @@@         
             ,,,,,,  (@@@@@@@@@@@@@  @@@@@@@    @@@         
            @@    @@   @@*  @@@      @@@    @@@@@@@         
            @@***#@@   @@*  @@@      @@@@@@@@               
                @@#    @@@@@@@@@@@@@@@@@                    
                @@#         @@@                             
                 &@@@@@@  @@@@/                             
                       @@@@                                 
                        @@@                                 
                        @@@                                 
                        @@@            
 
LOGO;

        $_filename = '/home/teodor/WebTree-m-'.time().'.sql';
        $output->writeln('<info>'.$logo.'</info>');

        $ml = new MigrationLogger( $_filename );

        R::getDatabaseAdapter()
            ->getDatabase()
            ->setLogger($ml)
            ->setEnableLogging(TRUE);


//        for($x=1;$x<=100;$x++){
//            $this->show_status($x, 100);
//            usleep(100000);
//        }

        $iloop = "0"; /* Outside the loop */
        while (true){
            $warn = "Program running hold on!!\r";
            if (strlen($warn) === $iloop+1){
                $iloop = "0";
            }
            $warn = str_split($warn);
            $iloop++;
            $warn[$iloop] = "\033[35;2m\e[0m".strtoupper($warn[$iloop]);
            echo " \033[7m".implode($warn);
            usleep(90000);
        }

        $output->writeln('<comment>Migrated into '.$_filename .'</comment>');


    }


    function show_status($done, $total, $size=30) {

        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time)) $start_time=time();
        $now = time();

        $perc=(double)($done/$total);

        $bar=floor($perc*$size);

        $status_bar="\r[";
        $status_bar.=str_repeat("=", $bar);
        if($bar<$size){
            $status_bar.=">";
            $status_bar.=str_repeat(" ", $size-$bar);
        } else {
            $status_bar.="=";
        }

        $disp=number_format($perc*100, 0);

        $status_bar.="] $disp%  $done/$total";

        $rate = ($now-$start_time)/$done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

        echo "$status_bar  ";

        flush();

        // when done, send a newline
        if($done == $total) {
            echo "\n";
        }

    }
}