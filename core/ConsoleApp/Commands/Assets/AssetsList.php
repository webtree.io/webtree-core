<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/9/19
 * Time: 2:16 AM
 */

namespace WebTree\ConsoleApp\Commands\Assets;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\lib\int\HQ\Assets\Assets;
use WebTree\SecOps;
use WebTree\WebTree as webtree;
use GuzzleHttp\Client as client;

class AssetsList extends Command
{
    protected function configure()
    {
        $this->setName('assets:list')
            ->setAliases(['a:l'])
            ->setDescription('List all available static libraries')
            ->setHelp('List all available static libraries');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $exit = Assets::getWTAssets();
        if (isset($exit) && !empty($exit)) {
            foreach ($exit as $item) {
                $output->writeln('<info>'.$item.'</info>');
            }
        } else {
            $output->writeln('<comment>ERROR</comment>');
        }
    }

}