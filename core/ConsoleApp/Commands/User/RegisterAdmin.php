<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/12/19
 * Time: 10:03 PM
 */

namespace WebTree\ConsoleApp\Commands\User;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\SecOps;
use WebTree\WebTree;
use WebTree\Logs as logs;
use WebTree\User as user;

class RegisterAdmin extends Command
{
    protected function configure()
    {
        $this->setName('user:reg')
            ->setAliases(['u:r'])
            ->setDescription('Register an user')
            ->setHelp('bla bla')
            ->addArgument('username', InputArgument::REQUIRED, 'username.')
            ->addArgument('password', InputArgument::REQUIRED, 'password.')
            ->addArgument('email', InputArgument::REQUIRED, 'email.')
            ->addArgument('type', InputArgument::REQUIRED, 'type.');
        ;
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logo = <<<LOGO
                        ,@@@@@@                             
                 .((((. @@    @# @@@@@@@@@@@@@@             
                 @(  .@ @@%%%%@# @@@        @@@             
                                 @@@        @@@             
                 @@@@@@@@@@@@@@  @@(        @@@             
                 @@@        ,@@  @@@@@@@@@@@@@@ @   @       
         @    (@ @@@        ,@@                 @@@@@       
        *@    (@ @@@        ,@@ @@    @@ @@@@@@@@@@@@@@     
                 @@@@@@@@@@@@@@ @@,,,,@@ @@@        @@@     
      @@@@@@@@@@@@@@                     @@@        @@@     
      @@@        &@@ /@@@@@@@@@@@@@.     @@#        @@@     
      @@@        &@@ %@@         @@.     @@@@@@@@@@@@@@     
      @@@        &@@ @@@         @@. @@@@@@@    @@@         
      @@@@@@@@@@@@@@ @@@*********@@. @     @    @@@         
             ,,,,,,  (@@@@@@@@@@@@@  @@@@@@@    @@@         
            @@    @@   @@*  @@@      @@@    @@@@@@@         
            @@***#@@   @@*  @@@      @@@@@@@@               
                @@#    @@@@@@@@@@@@@@@@@                    
                @@#         @@@                             
                 &@@@@@@  @@@@/                             
                       @@@@                                 
                        @@@                                 
                        @@@                                 
                        @@@             
 
LOGO;
        $usr = new user();

        $username = $input->getArgument('username');
        $pwd = $input->getArgument('password');
        $email = $input->getArgument('email');
        $type = $input->getArgument('type');
        $data = array(
            'username' => $username,
            'password' => $pwd,
            'email' => $email,
            'type' => $type
        );
        if ($usr->regAdmin($data)) {
            $output->writeln('<info>'.$logo.'</info>');
            $output->writeln('<info>'.$username.' has been registered</info>');
        } else {
            $output->writeln('<comment>Error registering user</comment>');
        }

    }
}