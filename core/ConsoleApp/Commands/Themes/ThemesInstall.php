<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/9/19
 * Time: 2:16 AM
 */

namespace WebTree\ConsoleApp\Commands\Themes;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\lib\int\HQ\Assets\Assets;
use WebTree\lib\int\HQ\Libs\Libs;
use WebTree\SecOps;
use WebTree\WebTree as webtree;
use GuzzleHttp\Client as client;
use function Siler\Str\lines;

class ThemesInstall extends Command
{
    protected function configure()
    {
        $this->setName('themes:install')
            ->setAliases(['t:i'])
            ->setDescription('Install theme')
            ->setHelp('provide theme name')
            ->addArgument('name', InputArgument::REQUIRED, 'provide theme name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $themeName = $input->getArgument('name');
        $themeNameArr = explode(' ', $themeName);

        $client = new client();
        $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/themes/'.$themeNameArr[0], []);
        $exit = json_decode($res->getBody());
        $output->writeln('<comment>Installing '.$themeNameArr[0].'</comment>');
        if (isset($exit) && !empty($exit)) {
            $iX = 1;
            $xLenght = count($exit);
            foreach ($exit as $item) {

                $output->writeln('<info>'.$this->show_status($iX, $xLenght).'</info>');

                $url = "https://webtree.iridiumintel.com/storage/themes/$item";

                $pathArray = explode("/", $item);
                $pathCounter = 0;
                foreach ($pathArray as &$path) {
                    if ($path == basename($url)) {
                        unset($pathArray[$pathCounter]);
                    }
                    $pathCounter++;
                }
                $pathStr = implode("/", $pathArray);

                if (!file_exists(THEPATH."static/themes/$pathStr")){
                    mkdir(THEPATH."static/themes/$pathStr", 0755, true);
                }
                $file_name = THEPATH."static/themes/$pathStr/".basename($url);

                if(file_put_contents($file_name, @file_get_contents($url))) {
                    $output->writeln('<info>'.basename($url).'</info>');
                }
                else {
                    $output->writeln('<comment>File downloading failed: '.basename($url).'</comment>');
                }
                $iX++;


            }


            // depending static libs
            $output->writeln('<info>Checking themes static libs</info>');
            $themeChecker = new \WebTree\lib\int\Themes\ThemeChecker();
            $libsHQ = new Libs();
            $libs = $themeChecker->getThemesLibs($themeName);
            $neededLibs = count($libs);
            $missingLibs = [];
            $li = 0;
            if (isset($libs) && !empty($libs)) {
                foreach ($libs as $lib) {
                    if (!$libsHQ->isStaticLibInstalled($lib)) {
                        $missingLibs[] = $libs[$li];
                    }
                    $li++;
                }
                $output->writeln('<comment>Theme using '.$neededLibs.' static libraries, '.count($missingLibs).' missing</comment>');
                if (count($missingLibs) > 0) {
                    $output->writeln('<info>Installing..</info>');

                        $command = $this->getApplication()->find('libs:static:install');
                        $names = implode(" ", $missingLibs);
                        $arguments = [
                            'command' => 'libs:static:install',
                            'name'    => $names,
                        ];

                        $greetInput = new ArrayInput($arguments);
                        try {
                            $returnCode = $command->run($greetInput, $output);
                        } catch (\Exception $e) {
                            $output->writeln('<comment>Error: '.$e.'</comment>');
                        }


                }
            }


            // depending assets
            $output->writeln('<info>Checking themes static assets</info>');
            $themeChecker = new \WebTree\lib\int\Themes\ThemeChecker();
            $assetsHQ = new Assets();
            $assets = $themeChecker->getThemesAssets($themeName);
            $neededAssets = count($assets);
            $missingAssets = [];
            $li = 0;
            if (isset($assets) && !empty($assets)) {
                foreach ($assets as $lib) {
                    if (!$assetsHQ->isAssetInstalled($lib)) {
                        $missingAssets[] = $assets[$li];
                    }
                    $li++;
                }
                $output->writeln('<comment>Theme using '.$neededAssets.' static assets, '.count($missingAssets).' missing</comment>');
                if (count($missingAssets) > 0) {
                    $output->writeln('<info>Installing..</info>');

                    $command = $this->getApplication()->find('assets:install');
                    $names = implode(" ", $missingAssets);
                    $arguments = [
                        'command' => 'assets:install',
                        'name'    => $names,
                    ];

                    $greetInput = new ArrayInput($arguments);
                    try {
                        $returnCode = $command->run($greetInput, $output);
                    } catch (\Exception $e) {
                        $output->writeln('<comment>Error: '.$e.'</comment>');
                    }


                }
            }

            $output->writeln('<comment>Theme downloaded successfully</comment>');


        } else {
            $output->writeln('<comment>ERROR</comment>');
        }



    }

    function show_status($done, $total, $size=30) {

        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time)) $start_time=time();
        $now = time();

        $perc=(double)($done/$total);

        $bar=floor($perc*$size);

        $status_bar="\r[";
        $status_bar.=str_repeat("=", $bar);
        if($bar<$size){
            $status_bar.=">";
            $status_bar.=str_repeat(" ", $size-$bar);
        } else {
            $status_bar.="=";
        }

        $disp=number_format($perc*100, 0);

        $status_bar.="] $disp%  $done/$total";

        $rate = ($now-$start_time)/$done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

        echo "$status_bar  ";

        flush();

        // when done, send a newline
        if($done == $total) {
            echo "\n";
        }

    }

}