<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/9/19
 * Time: 2:16 AM
 */

namespace WebTree\ConsoleApp\Commands\Themes;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\SecOps;
use WebTree\WebTree as webtree;
use GuzzleHttp\Client as client;

class ThemesList extends Command
{
    protected function configure()
    {
        $this->setName('themes:list')
            ->setAliases(['t:l'])
            ->setDescription('List all available themes')
            ->setHelp('List all available themes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $exit = \WebTree\lib\int\HQ\Themes\Themes::getWTThemes();
        if (isset($exit) && !empty($exit)) {
            foreach ($exit as $item) {
                $output->writeln('<info>'.$item.'</info>');
            }
        } else {
            $output->writeln('<comment>ERROR</comment>');
        }
    }

}