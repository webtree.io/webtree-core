<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/9/19
 * Time: 2:16 AM
 */

namespace WebTree\ConsoleApp\Commands\Libs;

use GuzzleHttp\Client as client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\SecOps;
use WebTree\WebTree as webtree;


class LibStaticInstall extends Command
{
    protected function configure()
    {
        $this->setName('libs:static:install')
            ->setAliases(['l:s:i'])
            ->setDescription('Install static library')
            ->setHelp('provide library name; use "all" to install all static libraries')
            ->addArgument('name', InputArgument::REQUIRED, 'provide library name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $libName = $input->getArgument('name');
        $libName = ltrim(rtrim($libName));
        $libNameArr = explode(' ', $libName);

        if ($libNameArr[0] == 'all') {

            $client = new client();
            $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/libs', []);
            $exit = json_decode($res->getBody());
            if (isset($exit) && !empty($exit)) {
                foreach ($exit as $item) {
                    $output->writeln('<comment>Installing '.$item.'</comment>');

                    $resX = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/libs/'.$item, []);
                    $exitX = json_decode($resX->getBody());

                    if (isset($exitX) && !empty($exitX)) {
                        $iX = 1;
                        $xLenght = count($exitX);
                        foreach ($exitX as $itemX) {

                            $output->writeln('<info>'.$this->show_status($iX, $xLenght).'</info>');

                            $url = "https://webtree.iridiumintel.com/storage/lib/$itemX";

                            $pathArray = explode("/", $itemX);
                            $pathCounter = 0;
                            foreach ($pathArray as &$path) {
                                if ($path == basename($url)) {
                                    unset($pathArray[$pathCounter]);
                                }
                                $pathCounter++;
                            }
                            $pathStr = implode("/", $pathArray);

                            if (!file_exists(THEPATH."static/lib/$pathStr")){
                                mkdir(THEPATH."static/lib/$pathStr", 0755, true);
                            }
                            $file_name = THEPATH."static/lib/$pathStr/".basename($url);

                            if(file_put_contents($file_name, @file_get_contents($url))) {
                                $output->writeln('<info>done</info>');
                            }
                            else {
                                $output->writeln('<comment>File downloading failed</comment>');
                            }
                            $iX++;


                        }
                        $output->writeln('<comment>Static library downloaded successfully</comment>');

                    }



                }



            } else {
                $output->writeln('<comment>ERROR</comment>');
            }

        } else {

            foreach ($libNameArr as $lib) {

                $client = new client();
                $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/libs/'.$lib, []);
                $exit = json_decode($res->getBody());
                $output->writeln('<comment>Installing '.$lib.'</comment>');
                if (isset($exit) && !empty($exit)) {
                    $iX = 1;
                    $xLenght = count($exit);
                    foreach ($exit as $item) {

                        $output->writeln('<info>'.$this->show_status($iX, $xLenght).'</info>');

                        $url = "https://webtree.iridiumintel.com/storage/lib/$item";

                        $pathArray = explode("/", $item);
                        $pathCounter = 0;
                        foreach ($pathArray as &$path) {
                            if ($path == basename($url)) {
                                unset($pathArray[$pathCounter]);
                            }
                            $pathCounter++;
                        }
                        $pathStr = implode("/", $pathArray);

                        if (!file_exists(THEPATH."static/lib/$pathStr")){
                            mkdir(THEPATH."static/lib/$pathStr", 0755, true);
                        }
                        $file_name = THEPATH."static/lib/$pathStr/".basename($url);

                        if(file_put_contents($file_name, @file_get_contents($url))) {
                            $output->writeln('<info>'.basename($url).'</info>');
                        }
                        else {
                            $output->writeln('<comment>File downloading failed: '.basename($url).'</comment>');
                        }
                        $iX++;


                    }

                    $output->writeln('<comment>Static library downloaded successfully</comment>');


                } else {
                    $output->writeln('<comment>ERROR</comment>');
                }

            }




        }




    }

    function show_status($done, $total, $size=30) {

        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time)) $start_time=time();
        $now = time();

        $perc=(double)($done/$total);

        $bar=floor($perc*$size);

        $status_bar="\r[";
        $status_bar.=str_repeat("=", $bar);
        if($bar<$size){
            $status_bar.=">";
            $status_bar.=str_repeat(" ", $size-$bar);
        } else {
            $status_bar.="=";
        }

        $disp=number_format($perc*100, 0);

        $status_bar.="] $disp%  $done/$total";

        $rate = ($now-$start_time)/$done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

        echo "$status_bar  ";

        flush();

        // when done, send a newline
        if($done == $total) {
            echo "\n";
        }

    }

}