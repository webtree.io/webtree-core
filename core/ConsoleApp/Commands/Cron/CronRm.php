<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/12/19
 * Time: 10:03 PM
 */

namespace WebTree\ConsoleApp\Commands\Cron;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\ConsoleApp\Commands\Crontab as crontab;

class CronRm extends Command
{
    protected function configure()
    {
        $this->setName('cron:rm')
            ->setDescription('Clears crontab')
            ->setHelp('bla bla')
            ->addArgument('command_to_rm', InputArgument::REQUIRED, 'Command to remove from crontab.');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logo = <<<LOGO

██╗██████╗ ██╗██████╗ ██╗██╗   ██╗███╗   ███╗    ██████╗  ██████╗ ██╗  ██╗
██║██╔══██╗██║██╔══██╗██║██║   ██║████╗ ████║    ██╔══██╗██╔═══██╗╚██╗██╔╝
██║██████╔╝██║██║  ██║██║██║   ██║██╔████╔██║    ██████╔╝██║   ██║ ╚███╔╝ 
██║██╔══██╗██║██║  ██║██║██║   ██║██║╚██╔╝██║    ██╔══██╗██║   ██║ ██╔██╗ 
██║██║  ██║██║██████╔╝██║╚██████╔╝██║ ╚═╝ ██║    ██████╔╝╚██████╔╝██╔╝ ██╗
╚═╝╚═╝  ╚═╝╚═╝╚═════╝ ╚═╝ ╚═════╝ ╚═╝     ╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═╝
                                                                          
LOGO;
        $ct = new crontab();
        $cronJob = $input->getArgument('command_to_rm');

        $exit = $ct->removeJob($cronJob);

        $output->writeln('<info>'.$exit.'</info>');

    }
}