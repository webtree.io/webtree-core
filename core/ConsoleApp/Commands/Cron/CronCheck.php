<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/12/19
 * Time: 10:03 PM
 */

namespace WebTree\ConsoleApp\Commands\Cron;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\SecOps;
use WebTree\WebTree;
use WebTree\Logs as logs;
//use WebTree\Cron as crontab;
use WebTree\ConsoleApp\Commands\Crontab as crontab;

class CronCheck extends Command
{
    protected function configure()
    {
        $this->setName('cron:check')
            ->setDescription('Checks crontab for specific job')
            ->setHelp('bla bla');
//            ->addArgument('command_to_check', InputArgument::REQUIRED, 'job to check for.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logo = <<<LOGO

██╗██████╗ ██╗██████╗ ██╗██╗   ██╗███╗   ███╗    ██████╗  ██████╗ ██╗  ██╗
██║██╔══██╗██║██╔══██╗██║██║   ██║████╗ ████║    ██╔══██╗██╔═══██╗╚██╗██╔╝
██║██████╔╝██║██║  ██║██║██║   ██║██╔████╔██║    ██████╔╝██║   ██║ ╚███╔╝ 
██║██╔══██╗██║██║  ██║██║██║   ██║██║╚██╔╝██║    ██╔══██╗██║   ██║ ██╔██╗ 
██║██║  ██║██║██████╔╝██║╚██████╔╝██║ ╚═╝ ██║    ██████╔╝╚██████╔╝██╔╝ ██╗
╚═╝╚═╝  ╚═╝╚═╝╚═════╝ ╚═╝ ╚═════╝ ╚═╝     ╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═╝
                                                                          
LOGO;
        $ct = new crontab();

        $exit = $ct->getJobs();

        if (empty($exit)) {
            $output->writeln('<comment>No registered jobs</comment>');
        } else {
            print_r($exit);
        }

    }
}