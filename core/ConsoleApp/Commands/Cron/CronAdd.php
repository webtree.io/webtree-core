<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/12/19
 * Time: 10:03 PM
 */

namespace WebTree\ConsoleApp\Commands\Cron;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use WebTree\SecOps;
use WebTree\WebTree;
use WebTree\Logs as logs;
//use WebTree\Cron as crontab;
use WebTree\ConsoleApp\Commands\Crontab as crontab;

class CronAdd extends Command
{
    protected function configure()
    {
        $this->setName('cron:add')
            ->setDescription('Add command to Cron')
            ->setHelp('bla bla')
            ->addArgument('command_to_add', InputArgument::REQUIRED, 'Command to add to cron.');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logo = <<<LOGO

██╗██████╗ ██╗██████╗ ██╗██╗   ██╗███╗   ███╗    ██████╗  ██████╗ ██╗  ██╗
██║██╔══██╗██║██╔══██╗██║██║   ██║████╗ ████║    ██╔══██╗██╔═══██╗╚██╗██╔╝
██║██████╔╝██║██║  ██║██║██║   ██║██╔████╔██║    ██████╔╝██║   ██║ ╚███╔╝ 
██║██╔══██╗██║██║  ██║██║██║   ██║██║╚██╔╝██║    ██╔══██╗██║   ██║ ██╔██╗ 
██║██║  ██║██║██████╔╝██║╚██████╔╝██║ ╚═╝ ██║    ██████╔╝╚██████╔╝██╔╝ ██╗
╚═╝╚═╝  ╚═╝╚═╝╚═════╝ ╚═╝ ╚═════╝ ╚═╝     ╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═╝

LOGO;
        $ct = new crontab();

        $cronJob = $input->getArgument('command_to_add');
        $exit = $ct->addJob($cronJob);
        $output->writeln('<info>'.$exit.'</info>');

    }
}