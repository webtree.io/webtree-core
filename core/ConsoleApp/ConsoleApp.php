<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/11/19
 * Time: 9:14 PM
 */

namespace WebTree\ConsoleApp;

use Symfony\Component\Console\Application;

class ConsoleApp extends Application
{
    private static $name = "WebTree CLI";
    /**
     * @var string
     */
    private static $logo = <<<LOGO
                        ,@@@@@@                             
                 .((((. @@    @# @@@@@@@@@@@@@@             
                 @(  .@ @@%%%%@# @@@        @@@             
                                 @@@        @@@             
                 @@@@@@@@@@@@@@  @@(        @@@             
                 @@@        ,@@  @@@@@@@@@@@@@@ @   @       
         @    (@ @@@        ,@@                 @@@@@       
        *@    (@ @@@        ,@@ @@    @@ @@@@@@@@@@@@@@     
                 @@@@@@@@@@@@@@ @@,,,,@@ @@@        @@@     
      @@@@@@@@@@@@@@                     @@@        @@@     
      @@@        &@@ /@@@@@@@@@@@@@.     @@#        @@@     
      @@@        &@@ %@@         @@.     @@@@@@@@@@@@@@     
      @@@        &@@ @@@         @@. @@@@@@@    @@@         
      @@@@@@@@@@@@@@ @@@*********@@. @     @    @@@         
             ,,,,,,  (@@@@@@@@@@@@@  @@@@@@@    @@@         
            @@    @@   @@*  @@@      @@@    @@@@@@@         
            @@***#@@   @@*  @@@      @@@@@@@@               
                @@#    @@@@@@@@@@@@@@@@@                    
                @@#         @@@                             
                 &@@@@@@  @@@@/                             
                       @@@@                                 
                        @@@                                 
                        @@@                                 
                        @@@            
 
LOGO;


    /**
     * MyApp constructor.
     *
     * @param string $name
     * @param string $version
     */
    public function __construct( $name = 'UNKNOWN', $version = 'UNKNOWN')
    {

        $this->setName(static::$name);
        $this->setVersion($version);

        parent::__construct($name, $version);

    }

    /**
     * @return string
     */
    public function getHelp()
    {
        return static::$logo . parent::getHelp();
    }
}