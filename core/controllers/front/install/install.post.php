<?php
use Siler\Http\Response;
use Siler\Twig;
use Siler\Http\Request;
use WebTree\SecOps;
use WebTree\Installer;
use WebTree\Heimdall as heimdall;

heimdall\guard('install');

$context = WebTree\WebTree::context();
$redata = $context['request']['post'];
$secops = new SecOps();
$installer = new Installer();

if (isset($redata) && !empty($redata)) {

    isset($redata['db_type']) ? $_db_type = strip_tags($redata['db_type']) : $_db_type = null;
    isset($redata['dbname']) ? $_db_name = strip_tags($redata['dbname']) : $_db_name = null;
    isset($redata['dbuname']) ? $_db_user = strip_tags($redata['dbuname']) : $_db_user = null;
    isset($redata['dbpwd']) ? $_db_pwd = $redata['dbpwd'] : $_db_pwd = null;
    isset($redata['dbhost']) ? $_db_host = strip_tags($redata['dbhost']) : $_db_host = null;

    isset($redata['site_title']) ? $_site_title = strip_tags($redata['site_title']) : $_site_title = null;

    isset($redata['adminuname']) ? $_admin_user = strip_tags($redata['adminuname']) : $_admin_user = null;
    isset($redata['adminemail']) ? $_admin_email = strip_tags($redata['adminemail']) : $_admin_email = null;
    isset($redata['adminpwd']) ? $_admin_pwd = $redata['adminpwd'] : $_admin_pwd = null;

    $data['db_type'] = $_db_type;
    $data['db_name'] = $_db_name;
    $data['db_user'] = $_db_user;
    $data['db_pwd'] = $_db_pwd;
    $data['db_host'] = $_db_host;

    $data['site_title'] = $_site_title;

    $data['admin_user'] = $_admin_user;
    $data['admin_email'] = $_admin_email;
    $data['admin_pwd'] = $_admin_pwd;


    if (isset($_db_type, $_db_name, $_db_user, $_db_pwd, $_db_host, $_site_title, $_admin_user, $_admin_email, $_admin_pwd)) {
        $rsp = $installer->WebTreeInit($data);
        $context['rsp'] = $rsp;
    }
}

try {
    $html = Twig\render('install.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);
