<?php
use Siler\Http\Response;
use Siler\Twig;
use WebTree\Heimdall as heimdall;

heimdall\guard('install');

$context = WebTree\WebTree::context();

try {
    $html = Twig\render('install.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);