<?php
use Siler\Http\Response;
use Siler\Twig;
use WebTree\Tangle as tangle;

$start = microtime();
$context = WebTree\WebTree::context();
$tangle = new tangle();

$tangle->hooks->add_action('wt_head', 'foo'); // todo
function foo() {
    return 'bar';
}





$end = microtime();
@$creationtime = ($end - $start) / 1000;
$context['render_time'] = number_format((float)$creationtime, 5, '.', '').' seconds';
try {
    $html = Twig\render('front/home.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);