<?php
use Siler\Http\Response;
use WebTree\Tangle as tangle;

$start = microtime();
$context = WebTree\WebTree::context();
$tangle = new tangle();

$tangle->hooks->add_action('wt_head', 'foo'); // todo
function foo() {
    return 'bar';
}


$end = microtime();
@$creationtime = ($end - $start) / 1000;
$context['render_time'] = number_format((float)$creationtime, 5, '.', '').' seconds';
Response\json(['ping'=>'pong']);