<?php

use Siler\Http\Response;
use WebTree\Multipass;
use WebTree\SecOps;

$mp = new multipass();
$secops = new secops();
$validator = new \WebTree\Validator();

$context = WebTree\WebTree::context();


if (!empty($context['request']['post'])) {
    $data = $context['request']['post'];
} elseif (!empty($context['request']['json'])) {
    $data = $context['request']['json'];
}

$data = $validator->userAuthObj($data);

if ($data) {
    $rsp = [];
    $rsp = $mp->apiLogin($data, 'user');
    $bad_ip = new \bad_ip\bad_ip();
    if (!empty($rsp)) {
        if (isset($rsp['error'])) {
            $context['notifications'] = [$rsp['error']];
        } else {
            if (isset($rsp['token'])) {
                $bad_ip->loginHook(true); // login success
            }
        }
    } else {
        $bad_ip->loginHook(); // login failed
        $context['notifications'] = ['Error in login process'];
    }
}

Response\json($rsp);