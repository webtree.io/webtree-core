<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/7/19
 * Time: 12:45 AM
 */


use Siler\Twig;
use Siler\Http\Response;
use WebTree\Heimdall as heimdall;
use WebTree\Multipass as multipass;
use WebTree\SecOps as secops;

heimdall\guard('login');

$mp = new multipass();
$secops = new secops();
$validator = new \WebTree\Validator();

$context = WebTree\WebTree::context();


if (!empty($context['request']['post'])) {
    $data = $context['request']['post'];
} elseif (!empty($context['request']['json'])) {
    $data = $context['request']['json'];
}

$data = $validator->userAuthObj($data);

if ($data) {

    $rsp = $mp->webLogin($data, 'user');
    $bad_ip = new \bad_ip\bad_ip();
    if ($rsp) {
        if (isset($rsp['error'])) {
            $context['notifications'] = [$rsp['error']];
        } else {
            if (isset($rsp['token'])) {
                $bad_ip->loginHook(true); // login success
                $secops->redirect('/user/dashboard', false);
            }
        }
    } else {
        $bad_ip->loginHook(); // login failed
        $context['notifications'] = ['Error in login process'];
    }
}


try {
    $html = Twig\render('user/login.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);
