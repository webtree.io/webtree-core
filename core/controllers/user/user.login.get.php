<?php



use Siler\Twig;
use Siler\Http\Response;
use WebTree\Heimdall as heimdall;

heimdall\guard('login');

$context = WebTree\WebTree::context();

try {
    $html = Twig\render('user/login.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);
