<?php

use Siler\Twig;
use Siler\Http\Response;
use WebTree\Heimdall as heimdall;
use WebTree\Themes;
use WebTree\SecOps;

heimdall\guard('', 'user');

$themes = new Themes();
$secops = new SecOps();
$context = WebTree\WebTree::context();


if (!empty($themes->get_themes())) {
    // todo
    $file_headers = array(
        'Name'        => 'Theme Name',
        'ThemeURI'    => 'Theme URI',
        'Description' => 'Description',
        'Author'      => 'Author',
        'AuthorURI'   => 'Author URI',
        'Version'     => 'Version',
        'Template'    => 'Template',
        'Status'      => 'Status',
        'Tags'        => 'Tags',
        'TextDomain'  => 'Text Domain',
        'DomainPath'  => 'Domain Path'
    );

    foreach ($themes->get_themes() as $theme) {
        $themeIndex = $theme['filepath'].'/index.php';

        if ($secops->exists($themeIndex)) {

            $themeData = $secops->get_file_data(  $themeIndex, $file_headers);
            $context['themes'][] = array(
                'theme' => $theme['name'],
                'data' => $themeData
            );
        }

    }
}


try {
    $html = Twig\render('user/dashboard-themes.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);
