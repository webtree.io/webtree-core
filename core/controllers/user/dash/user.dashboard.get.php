<?php

use Siler\Twig;
use Siler\Http\Response;
use WebTree\Heimdall as heimdall;

heimdall\guard('', 'user');

$context = WebTree\WebTree::context();

if (!empty($context['request']['post'])) {
    $data = $context['request']['post'];
} elseif (!empty($context['request']['json'])) {
    $data = $context['request']['json'];
}


try {
    $html = Twig\render('user/dashboard.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);
