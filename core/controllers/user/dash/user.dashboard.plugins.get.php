<?php

use Siler\Twig;
use Siler\Http\Response;
use WebTree\Heimdall as heimdall;
use WebTree\Plugins;
use WebTree\SecOps;

heimdall\guard('', 'user');

$plugins = new Plugins();
$secops = new SecOps();
$context = WebTree\WebTree::context();


if (!empty($plugins->get_plugins())) {
    // todo
    $file_headers = array(
        'Name'        => 'Plugin Name',
        'PluginURI'    => 'Plugin URI',
        'Description' => 'Description',
        'Author'      => 'Author',
        'AuthorURI'   => 'Author URI',
        'Version'     => 'Version',
        'Template'    => 'Template',
        'Status'      => 'Status',
        'Tags'        => 'Tags',
        'TextDomain'  => 'Text Domain',
        'DomainPath'  => 'Domain Path'
    );

    foreach ($plugins->get_plugins() as $plugin) {
        $pluginIndex = $plugin['filepath'].'/index.php';

        if ($secops->exists($pluginIndex)) {

            $pluginData = $secops->get_file_data(  $pluginIndex, $file_headers);
            $context['plugins'][] = array(
                'plugin' => $plugin['name'],
                'data' => $pluginData
            );

        }

    }
}

try {
    $html = Twig\render('user/dashboard-plugins.twig', $context);
} catch (\Twig\Error\LoaderError $e) {
} catch (\Twig\Error\RuntimeError $e) {
} catch (\Twig\Error\SyntaxError $e) {
}
Response\html($html);
