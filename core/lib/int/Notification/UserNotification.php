<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 8/4/19
 * Time: 9:24 PM
 */

namespace WebTree;

use RedBeanPHP\Facade as R;
use RedBeanPHP\RedException\SQL;
use WebTree\lib\int\Notification\models\UserNotification as notificationMod;

class UserNotification
{

    private $user;
    private $currentUser;

    /**
     * ExpertNotification constructor.
     */
    public function __construct()
    {
        $userObj = new \WebTree\User();
        $this->user = $userObj;
        $this->currentUser = $userObj->currentUser;  // todo pa ne moze svuda current jbt...
    }


    public function notifyMe(array $data) {

        $response = null;
        if (!empty($data) && isset($data['user_id'])) {  //(isset($this->currentUser->id) ||
            $write = false;
            $_now = time();
            $_today = date("Y-m-d");

//            $dbseed = R::load('usernotification', 0);
//            empty($dbseed) ? $seed = true : $seed = false;

            $uID = $this->user->getUser($data['user_id']);
            $expertNotification = new notificationMod();
            $notification = R::dispense( 'usernotification' );
            $notification->created_ts = time();
            $notification->updated_ts = null;
            $notification->input_date = $_today;
            $notification->seen = 0;

            foreach ($data as $index => $value) {

                if (in_array($index, $expertNotification->index) && isset($xpID->id)) {
                    $notification[$index] = $value;
                    $write = true;
                }
            }

            if ($write) {
                $uID->xownUsernotificationList[] = $notification;
                try {
                    R::store($uID);
                } catch (SQL $e) {
                    //echo $e;
                }

                $response[] = 'Expert Notification added';
            }


        }

        return $response;

    }


    public function iVeSeen($id) {
        $uid = $this->currentUser->id;
        if (isset($uid)) {
            $notification = R::load( 'usernotification', $id );
            if ($notification->user_id == $this->currentUser->id) {
                $notification->seen = 1;
                $notification->updated_ts = time();
                try {
                    R::store($notification);
                } catch (SQL $e) {
                    //echo $e;
                }
            }
        }

    }


    public function haveNewNotifications() {
        $uid = $this->currentUser->id;
        if (isset($uid)) {
            $all = R::findOne('usernotification',' user_id = ? AND seen = ? ', [$uid, 0]);
            return !empty($all) ? true : null;
        }
        return null;
    }


    public function getMyNotifications($page = 1, $limit = 10) {
        $uid = $this->currentUser->id;
        if (isset($uid)) {
            $all = R::findAll('usernotification',' user_id = ? ORDER BY id DESC LIMIT '.(($page-1)*$limit).', '.$limit, [$uid]);
            return !empty($all) ? $all : null;
        }
        return null;
    }

    public function getMyNotificationByID($id) {
        $uid = $this->currentUser->id;
        if (isset($uid)) {
            $all = R::findOne('usernotification',' user_id = ? AND id = ? ', [$uid, $id]);
            return !empty($all) ? $all : null;
        }
        return null;
    }


    public function getNotification0() {
        $all = R::findAll('notification0');
        return !empty($all) ? $all : null;
    }


    public function getNotificationsNotification0($src = '') {

        if ($src !== '') {
            $xpArray = explode(',', $src);
            if (!empty($xpArray)) {
                $notification0 = self::getNotification0();
//                $exit = [];
                foreach ($notification0 as $xp0) {
                    foreach ($xpArray as $xparr0) {
                        if ($xparr0 == $xp0['id']) {
                            $exit[] = $xp0;
                        }
                    }
                }
            }
            return !empty($exit) ? $exit : null;
        }
        return null;
    }


}