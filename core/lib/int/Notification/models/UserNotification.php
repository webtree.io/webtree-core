<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 8/4/19
 * Time: 9:29 PM
 */

namespace WebTree\lib\int\Notification\models;


class UserNotification
{

    public $index;

    /**
     * ExpertNotification constructor.
     */
    public function __construct()
    {
        $this->index = [
            'text', 'source', 'group', 'seen', 'type', 'action', 'user_id'
        ];
    }
}