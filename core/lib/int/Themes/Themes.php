<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/19/19
 * Time: 10:58 PM
 */

namespace WebTree;

//function __autoload($class) {
//    include(getcwd() . "/ext/themes/" . ucfirst($class) . ".php");
//}

/*
   Static class responsible for every theme
*/
class Themes {

    private static $themes = array();
    private static $hooks = array();
    private static $theme_count = 0;
    private static $hook_count = 0;

    public function __construct() {
        $disabled = ["~", "."];	// themes whose folders start with these characters will be treated as disabled.
        $dir = opendir(getcwd() . '/ext/themes');
        while (false !== ($filename = readdir($dir))) {
//            if (stripos($filename, '.php') === false || strcmp($filename, 'Itheme.php') == 0)
//                continue;
            if ($filename == "." || $filename == "..")
                continue;

            if($filename === false)
                continue;

            $fc = substr($filename, 0, 1);
            if(in_array($fc, $disabled))
                continue;

            $name = str_replace('.php', '', $filename);
            array_push(self::$themes, array(
                    'name' => $name,
                    'filename' => $filename,
                    'filepath' => getcwd().'/ext/themes/'.$filename
                )
            );
            self::$theme_count++;
            self::add_hook($name, true);
        }
    }

    public static function add_hook($name, $activate) {
        $hooks = self::$hooks;
        $length = count($hooks);
        for ($i = 0; $i < $length; $i++)
            if (in_array($name, $hooks[$i]) == true) {
                if ($hooks[$i]['name'] !== $name)
                    continue;
                self::$hooks[$i]['activate'] = 1;
                self::$hook_count++;
                return;
            }
        array_push(self::$hooks, array(
                'name' => $name,
                'activate' => $activate
            )
        );
        self::$hook_count++;
    }

    public static function remove_hook($name) {
        $hooks = self::$hooks;
        $length = count($hooks);
        $name = str_replace('.php', '', $name);
        for ($i = 0; $i < $length; $i++) {
            if ($hooks[$i]['name'] !== $name)
                continue;

            self::$hook_count--;
            self::$hooks[$i]['activate'] = 0;
        }
    }

    public static function run_hooks($hook) {
        $themes = self::$themes;
        $hooks = self::$hooks;
        for ($i = 0; $i < count($hooks); $i++) {
            $theme = self::getthemeObject($hooks[$i]['name']);
            if ($hooks[$i]['activate'] == 0) {
                $hook_defunc = 'deactivate_' . $hook;
                $func = is_callable(array($theme, $hook_defunc));
                if (!$func)
                    $theme->deactivate();
                else
                    $theme->$hook_defunc();
                continue;
            }
            $theme_hooks = count($theme->set_hooks());
            for ($i2 = 0; $i2 < $theme_hooks; $i2++)
                if (strcasecmp($theme_hooks[$i2], $hook) != 0)
                    continue;

            $hook_func = 'activate_' . $hook;
            $func = is_callable(array($theme, $hook_func));
            if (!$func)
                $theme->deactivate();
            else
                $theme->$hook_func();
        }
    }

    public static function getthemeObject($name) {
        $themes = self::$themes;
        $theme_count = count($themes);
        for ($i = 0; $i < $theme_count; $i++) {
            if ($themes[$i]['name'] !== $name)
                continue;
            return new $themes[$i]['name']();
        }

        return null;
    }

    public static function getthemeCount() {
        return self::$theme_count;
    }

    public static function getHookCount() {
        return self::$hook_count;
    }

    public static function get_themes() {
        return self::$themes;
    }

    public static function get_hooks() {
        return self::$hooks;
    }

}