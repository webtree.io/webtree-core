<?php


namespace WebTree\lib\int\Themes;


use WebTree\ToolBox;

class ThemeChecker //todo decoupled class?
{

    /**
     * ThemeChecker constructor.
     */
    public function __construct()
    {
    }


    public function isThemeInstalled($name) {
        $themesArr = self::getThemeNames();
        return in_array($name, $themesArr) ? true : false;
    }

    public function getThemes() {
        $tools = new ToolBox();
        $assets = $tools->get_filelist_as_array(THEPATH.'static/themes'); //todo
        return isset($assets) ? $assets : null;
    }

    public function getThemeNames() {
        $assets = self::getThemes();
        $exit = [];
        foreach ($assets as $asset) {
            $assetArr = explode("/", $asset);
            if (!in_array($assetArr[0], $exit)) {
                $exit[] = $assetArr[0];
            }
        }
        return $exit;
    }

    public function getThemesConfig($theme) {
        $config_file = THEPATH.'static/themes/'.$theme.'/theme.wt';
        // Parse with sections
        $ini_array = [];
        if (file_exists($config_file)) {
            $ini_array = parse_ini_file($config_file, true);
        }
        return $ini_array;
    }

    public function getThemesLibs($theme) {
        $configArr = self::getThemesConfig($theme);
        if (isset($configArr, $configArr['static']['libs']) && !empty($configArr['static']['libs'])) {
            $libsArr = explode(",", trim($configArr['static']['libs']));
        }
        return isset($libsArr) ? $libsArr : null;
    }

    public function getThemesAssets($theme) {
        $configArr = self::getThemesConfig($theme);
        if (isset($configArr, $configArr['static']['assets']) && !empty($configArr['static']['assets'])) {
            $libsArr = explode(",", trim($configArr['static']['assets']));
        }
        return isset($libsArr) ? $libsArr : null;
    }

}