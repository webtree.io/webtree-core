<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 9/4/19
 * Time: 1:24 PM
 */

namespace WebTree;


use RedBeanPHP\Logger;

class MigrationLogger implements Logger
{
    private $file;

    /**
     * MigrationLogger constructor.
     * @param $file
     */
    public function __construct( $file ) {
        $this->file = $file;
    }


    public function log() {
        $query = func_get_arg(0);
        if (preg_match( '/^(CREATE|ALTER)/', $query )) {
            file_put_contents( $this->file, "{$query};\n",  FILE_APPEND );
        }
    }


}