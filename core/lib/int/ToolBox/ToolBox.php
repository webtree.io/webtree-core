<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/17/19
 * Time: 11:32 PM
 */

namespace WebTree;


use DateTime;
use DirectoryIterator;
use RedBeanPHP\Facade as R;


class ToolBox
{

    /**
     * ToolBox constructor.
     */
    public function __construct()
    {
    }

    public static function getVendorDir() {
        $reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
        $vendorDir = dirname($reflection->getFileName(), 2);
        return $vendorDir;
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function hasSeed($table) { //todo
        $dbseed = R::load($table, 0);
        return empty($dbseed) ? false : true;
    }

    public function isEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) ? true : false;
    }

    public function returnEmailUsername($email) {
        if (self::isEmail($email)) {
            $emailArr = explode("@", $email);
            return $emailArr[0];
        }
        return null;
    }

    public static function downloadFile($url, $filename, $extension, $destination = THEPATH) {
        $ch = curl_init();
        $source = $url;
        curl_setopt($ch, CURLOPT_URL, $source);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec ($ch);
        curl_close ($ch);

        $destination = $destination.$filename.$extension;
        $file = fopen($destination, "w+");
        fputs($file, $data);
        fclose($file);
    }

    public function countryList() {

        $dbseed = R::load('country', 0);
        empty($dbseed) ? $seed = true : $seed = false;

        if (!$seed) {
            $all = R::findAll('country');
            return $all;
        }
        return null;
    }

    // Return an array with the list of sub directories of $dir
    public static function getSubDirectories($dir, $absolute = true)
    {
        $subDir = array();
        // Get and add directories of $dir
        $directories = array_filter(glob($dir), 'is_dir');
        $subDir = array_merge($subDir, $directories);
        // Foreach directory, recursively get and add sub directories
        foreach ($directories as $directory) $subDir = array_merge($subDir, self::getSubDirectories($directory.'/*'));

        if (!$absolute) {
            $dirsStr = implode(",", $subDir);
            $dirsStr = str_replace(THEPATH, "",$dirsStr);
            $subDir = explode(",", $dirsStr);
        }

        // Return list of sub directories
        return $subDir;
    }

    public function get_filelist_as_array($dir, $recursive = true, $basedir = '', $include_dirs = false) {
        if ($dir == '') {return array();} else {$results = array(); $subresults = array();}
        if (!is_dir($dir)) {$dir = dirname($dir);} // so a files path can be sent
        if ($basedir == '') {$basedir = realpath($dir).DIRECTORY_SEPARATOR;}

        $files = scandir($dir);
        foreach ($files as $key => $value){
            if ( ($value != '.') && ($value != '..') ) {
                $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
                if (is_dir($path)) {
                    // optionally include directories in file list
                    if ($include_dirs) {$subresults[] = str_replace($basedir, '', $path);}
                    // optionally get file list for all subdirectories
                    if ($recursive) {
                        $subdirresults = $this->get_filelist_as_array($path, $recursive, $basedir, $include_dirs);
                        $results = array_merge($results, $subdirresults);
                    }
                } else {
                    // strip basedir and add to subarray to separate file list
                    $subresults[] = str_replace($basedir, '', $path);
                }
            }
        }
        // merge the subarray to give the list of files then subdirectory files
        if (count($subresults) > 0) {$results = array_merge($subresults, $results);}
        return $results;
    }

    public function uploadProfileImage($uid, $image) {
        $path_parts = pathinfo($image["name"]);
        $extension = $path_parts['extension'];
        if (!file_exists('./uploads/'.$uid.'/img/profile')) {
            self::createPath('./uploads/'.$uid.'/img/profile');
        }

        $imagename = 'profile.'.$extension;
        $source = $image['tmp_name'];
        $target = './uploads/'.$uid.'/img/profile/'.$imagename;

        $currentImg = self::getUserProfileImage($uid);
        isset($currentImg['path']) ? unlink($currentImg['path']) : null; //todo connected with "//todo :D"
        move_uploaded_file($source, $target);

        $imagepath = $imagename;
        $save = './uploads/'.$uid.'/img/profile/' . $imagepath; //This is the new file you saving
        $file = './uploads/'.$uid.'/img/profile/' . $imagepath; //This is the original file

        list($width, $height) = getimagesize($file);

        $tn = imagecreatetruecolor($width, $height);

        //$image = imagecreatefromjpeg($file);
        $info = getimagesize($target);
        if ($info['mime'] == 'image/jpeg'){
            $image = imagecreatefromjpeg($file);
        }elseif ($info['mime'] == 'image/gif'){
            $image = imagecreatefromgif($file);
        }elseif ($info['mime'] == 'image/png'){
            $image = imagecreatefrompng($file);
        }

        imagecopyresampled($tn, $image, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tn, $save, 80);
    }

    public function getUserProfileImage($uid) {
        $directory = THEPATH.'uploads/'.$uid.'/img/profile/';
        $files = [];
        file_exists($directory) ? $files = scandir ($directory) : null;
        if (isset($files[2])) {
            $firstFile = $directory . $files[2]; //todo :D
            $info = getimagesize($firstFile);
            if ($info['mime'] == 'image/jpeg'){
            }elseif ($info['mime'] == 'image/gif'){
            }elseif ($info['mime'] == 'image/png'){
            } else {
                return null;
            }
            $exit['path'] = $firstFile;
            $exit['url'] = '/uploads/'.$uid.'/img/profile/'.$files[2];
            return $exit;
        } else {
            $exit['path'] = THEPATH.'static/assets/images/users/no-photo.png';
            $exit['url'] = '/static/assets/images/users/no-photo.png';
            return $exit;
        }
    }

    /**
     * recursively create a long directory path
     */
    public function createPath($path) {
        if (is_dir($path)) return true;
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        $return = self::createPath($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }


}