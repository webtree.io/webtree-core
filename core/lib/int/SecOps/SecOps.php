<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/16/19
 * Time: 1:52 AM
 */

namespace WebTree;


class SecOps
{

    public $theRoute;
    public $configContainer;


    /**
     * SecOps constructor.
     */
    public function __construct()
    {
        @$this->theRoute = $_SERVER['REQUEST_URI'];
        @$this->configContainer = self::get_config();

    }


    /**
     * Strips htmlspecialchars & tags
     * @param $value
     * @return string
     */
    public function sanitizer($value) {
        return htmlspecialchars(strip_tags($value));
    }

    public function exists($fn) {
        $filename = strip_tags($fn); //todo ?
        return file_exists($filename) ? true : false;
    }

    public function redirect($url, $permanent = false) {
        header('Location: ' . $url, true, $permanent ? 301 : 302);
        exit();
    }

    public function get_config($config_file = THEPATH.'.webtree.ini.php') {
        // Parse with sections
        $ini_array = [];
        if (self::exists($config_file)) {
            $ini_array = parse_ini_file($config_file, true);
        }
        return $ini_array;
    }


    /**
     * Retrieve metadata from a file.
     *
     * Searches for metadata in the first 8kiB of a file, such as a plugin or theme.
     * Each piece of metadata must be on its own line. Fields can not span multiple
     * lines, the value will get cut at the end of the first line.
     *
     * If the file data is not within that first 8kiB, then the author should correct
     * their plugin file and move the data headers to the top.
     *
     * @param string $file Path to the file
     * @param array $default_headers List of headers, in the format array('HeaderKey' => 'Header Name')
     * @return array
     */

    function get_file_data( $file, $default_headers) {

        $fp = fopen( $file, 'r' );
        $file_data = fread( $fp, 8192 );
        fclose( $fp );
        $file_data = str_replace( "\r", "\n", $file_data );
        $all_headers = $default_headers;

        foreach ( $all_headers as $field => $regex ) {
            if (preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match )
                && $match[1])
                $all_headers[ $field ] = trim(preg_replace("/\s*(?:\*\/|\?>).*/", '', $match[1]));
            else
                $all_headers[ $field ] = '';
        }

        return $all_headers;
    }


}