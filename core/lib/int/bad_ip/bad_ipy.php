<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 6/11/19
 * Time: 1:08 AM
 */

namespace WebTree;

use GuzzleHttp\Client as client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;

class bad_ipy
{

    public function getLatestInc($limit = 20) {


        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->get('https://bad-ip.info/api/v1/bad_ip/latest/?uid=test&limit='.$limit);
        return json_decode($response->getBody(), true);

    }

    public function checkIP($ip) {

        if (isset($ip)) {
            $client = new Client([
                'headers' => [ 'Content-Type' => 'application/json' ]
            ]);

            $response = $client->post('https://bad-ip.info/v1/bad_ip/check?input=test',
                ['body' => json_encode(
                    [
                        'ip' => $ip
                    ]
                )]
            );

            return json_decode($response->getBody(), true);
        }
        return false;

    }

}