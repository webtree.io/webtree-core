<?php


namespace WebTree\lib\int\Heimdall;


use RedBeanPHP\Facade as R;
use Tangle\TangleCookie;
use WebTree\SecOps as secops;
use function WebTree\Heimdall\get_user;
use function WebTree\Heimdall\redirect;

class OmniSense
{

    /**
     * OmniSense constructor.
     */
    public function __construct() //secops i lightuser?
    {
    }


    public function handleNatives($natives) {
        $secops = new SecOps();
        $lightUser = get_user();

        if (is_array($natives)) {
            foreach ($natives as $native) {
                if ($native !== $lightUser['type']) {
                    $secops->redirect("/$native/login", false);
                    die();
                }
            }
        } else {
            if ($natives) {
                if ($natives !== $lightUser['type']) {
                    $secops->redirect("/$natives/login", false);
                    die();
                }
            }
        }
    }

    public function handleInstall() {

    }

    public function handleEntrances($entrances) { //todo
        $secops = new SecOps();
        $lightUser = get_user();
        $tangle = new TangleCookie();

        if (is_array($entrances)) {
            foreach ($entrances as $entrance) {

                if (!$entrance) {
                    if (!$lightUser) { // todo && ruta != /
                        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                                "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        $tangle->addTangle('originURL', $actual_link);
                        redirect('/user/login', true);
                        die();
                    }
                }

                if ($entrance == 'install' && $secops->exists('.webtree.ini.php')) {
                    $site = R::load( 'core', 1 );
                    if (isset($site) && !empty($site) && isset($site->installed) && $site->installed == 1) {
                        redirect('/', false);
                        die();
                    }
                }

                if ($entrance == 'login') {
                    if ($lightUser) {
                        $tmpRoute = $lightUser['type'];
                        redirect("/$tmpRoute/dashboard", false);
                        die();
                    }
                }


            }
        } else {

            $entrance = $entrances;
            if (!$entrance) {
                if (!$lightUser) { // todo && ruta != /
                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                            "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $tangle->addTangle('originURL', $actual_link);
                    redirect('/user/login', true);
                    die();
                }
            }

            if ($entrance == 'install' && $secops->exists('.webtree.ini.php')) {
                $site = R::load( 'core', 1 );
                if (isset($site) && !empty($site) && isset($site->installed) && $site->installed == 1) {
                    redirect('/', false);
                    die();
                }
            }

            if ($entrance == 'login') {
                if ($lightUser) {
                    $tmpRoute = $lightUser['type'];
                    redirect("/$tmpRoute/dashboard", false);
                    die();
                }
            }


        }

    }

}