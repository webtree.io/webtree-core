<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/16/19
 * Time: 12:52 AM
 */

namespace WebTree\Heimdall;

use Siler\Twig;
use Firebase\JWT\JWT;
use Siler\Http as HTTP;
use Siler\Http\Response;
use Siler\Route;
use WebTree\lib\int\Heimdall\OmniSense;
use WebTree\Multipass;
use WebTree\SecOps as secops;
use RedBeanPHP\Facade as R;


/**
 * Get users id from cookie JWT token
 * @return |null
 */
function get_user(){

    $so = new secops();
    $configContainer = $so->configContainer;

    if (isset($configContainer) && !empty($configContainer)) {
        $secret_key = $configContainer['cookie']['secret_key'];
        $cookie_name = $configContainer['cookie']['name'];
        $bearer = Multipass::getBearer();
        $backMajor = isset($bearer) && !empty($bearer) ? $bearer : HTTP\cookie($cookie_name);
    }

    if(!isset($backMajor,$secret_key)) {

        return null;

    } else {

        try {
            $decoded = JWT::decode($backMajor, $secret_key, array('HS256'));
        } catch (\Error $e) {

        }
        finally {
            if (isset($decoded, $decoded->data->user->id)){
                $exit['id'] = $decoded->data->user->id;
                $exit['type'] = $decoded->data->user->type;
                return $exit;
            } else {
                return null;
            }
        }

    }
}

function getUserIP() {
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}

/**
 * @param $url
 * @param bool $permanent
 */
function redirect($url, $permanent = false) {
    header('Location: ' . $url, true, $permanent ? 301 : 302);
    exit();
}

/**
 * Guards the entrance
 * @param string|array $entrance
 * @param string|array $natives
 */
function guard($entrance = null, $natives = null) {

    $secops = new SecOps();
    $omniSense = new OmniSense();

    $omniSense->handleEntrances($entrance);
    $omniSense->handleNatives($natives);


    // Install
    if (!$secops->exists('.webtree.ini.php')) {
        if ($secops->theRoute !== '/install') {
            redirect('/install', false);
            die();
        }

    }

}

/**
 * Handle logout
 * @param null $handle
 */
function log_out($handle = null) {
    // todo cookie?
    $so = new secops();
    $configContainer = $so->configContainer;

    if (isset($configContainer) && !empty($configContainer)) {
        $cookie_name = $configContainer['cookie']['name'];
        unset($_COOKIE[$cookie_name]);

        // empty value and expiration one hour before
        setcookie($cookie_name, '', time() - 3600, '/');
    }

    header('Location: ' . '/user/login', true, 301);
    exit();
}