<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 7/19/19
 * Time: 12:39 AM
 */

namespace WebTree;


use Firebase\JWT\JWT;
use RedBeanPHP\RedException\SQL;
use WebTree\Log\Logger as logger;
use RedBeanPHP\Facade as R;
use WebTree\SecOps as secops;

class Multipass
{

    protected $secret_key;
    protected $cookie_name;
    protected $cookie_hash;
    protected $token_days;
    protected $logger;
    protected $validator;
    protected $tools;

    /**
     * Multipass constructor.
     */
    public function __construct() {
        $so = new secops();
        $configContainer = $so->configContainer;

        if (isset($configContainer) && !empty($configContainer)) {
            $this->secret_key = $configContainer['cookie']['secret_key'];
            $this->cookie_name = $configContainer['cookie']['name'];
            $this->token_days = $configContainer['variables']['token_days'];
        }

        $this->cookie_hash = uniqid(md5(mt_rand()));
        $this->logger = new logger();
        $this->validator = new Validator();
        $this->tools = new ToolBox();

    }

    public static function getBearer() {
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) $bearer = $_SERVER['HTTP_AUTHORIZATION'];
        if (isset($bearer)) {
            $bearer = str_ireplace('bearer', '', $bearer);
            $bearer = preg_replace('/\s+/', '', $bearer);
        }
        return isset($bearer) ? $bearer : null;
    }

    public function generateUserToken($user) {

        if (isset($user, $user['id'], $user['type'])) {
            /** Valid credentials, the user exists create the according Token */
            $issuedAt = time();
            $notBefore = $issuedAt;
            $expire = $issuedAt + (86400 * (int)$this->token_days); //todo centralizuj odavde i ubaci u filter editabilni

            $rawToken = array(
                'iss' => 'https://webtree.iridiumintel.com',
                'iat' => $issuedAt,
                'nbf' => $notBefore,
                'exp' => $expire,
                'data' => array(
                    'user' => array(
                        'id' => $user['id'],
                        'type' => $user['type']
                    ),
                ),
            );
            /** Let the user modify the token data before the sign. */
            //                $token = JWT::encode($token, $secret_key);
            $token = $this->encode($rawToken);
            return [
                'raw'=> $rawToken,
                'token' => $token
            ];
        }

        return null;
    }

    public function decode($token) {

        try {
            $decoded = JWT::decode($token, $this->secret_key, array('HS256'));
        } catch (\Error $e) {

        }
        finally {
            if (isset($decoded)){
                return $decoded;
            } else {
                return null;
            }
        }

    }

    public function encode($token) {

        $_token = JWT::encode($token, $this->secret_key);
//        $this->logger->note('multipass', [$_token]);
        return isset($_token) ? $_token : null;
    }


    public function webLogin(array $data, $type) {

        $data = $this->validator->userAuthObj($data);
        $_username = $data['username'];
        $_password = $data['password'];
        $_email = $data['email'];


        if ($data && $type) {
            $response = [];
            $_code = 200;

            $isHereUser = null;
            $isHereEmail = null;

            if ($this->tools->hasSeed('user')) {
                $theUser = R::findOne($type, ' user_login = ? ', array($_username));
                if (empty($theUser)) {
                    $theUser = R::findOne($type, ' user_email = ? ', array($_email));
                }
            }


            if (isset($theUser) && !empty($theUser)) {

                $hash = $theUser->user_pass;
                $type = $theUser->type;

                if ($type) {

                    if (password_verify($_password, $hash)) {

                        /** Valid credentials, the user exists create the according Token */
                        $issuedAt = time();
                        $notBefore = $issuedAt;
                        $expire = $issuedAt + (86400 * 7); //todo centralizuj odavde i ubaci u filter editabilni

                        $token = array(
                            'iss' => 'http://example.org',
                            'iat' => $issuedAt,
                            'nbf' => $notBefore,
                            'exp' => $expire,
                            'data' => array(
                                'user' => array(
                                    'id' => $theUser->id,
                                    'type' => $theUser->type
                                ),
                            ),
                        );

                        /** Let the user modify the token data before the sign. */
                        //                $token = JWT::encode($token, $secret_key);
                        $token = $this->encode($token);

                        /** The token is signed, now create the object with no sensible user data to the client*/
                        $exitData = array(
                            'code' => $_code,
                            'token' => $token,
                            'user_email' => $theUser->user_email,
                            'type' => $type,
                        );

                        // Store User Token
                        $_token = R::dispense('tokens');
                        $_token->token = $token;
                        $_token->iss = 'http://example.org';
                        $_token->iat = $issuedAt;
                        $_token->nbf = $notBefore;
                        $_token->exp = $expire;

                        $theUser->xownTokenList[] = $_token;
                        try {
                            R::store($theUser);
                        } catch (SQL $e) { //todo
                        }

                        // Set the cookie
                        $cookie_value = $token;
                        setcookie($this->cookie_name, $cookie_value, time() + (86400 * 7), '/');

                        return $exitData;
                    } else {
                        $response['code'] = 1001;
                        $response['error'] = 'Wrong username or password';
                    }
                } else {
                    $response['code'] = 1000;
                    $response['error'] = 'User or type not found';
                }
                return $response;
            } else {
                return $response;
            }

        }
        return null;
    }


    public function apiLogin(array $data, $type) {
        $data = $this->validator->userAuthObj($data);
        $_username = strip_tags($data['username']);
        $_password = $data['password'];
        $_email = strip_tags($data['email']);


        if ($data && $type) {
            $response = [];
            $_code = 200;

            $isHereUser = null;
            $isHereEmail = null;

            if ($this->tools->hasSeed('user')) {
                $theUser = R::findOne($type, ' user_login = ? ', array($_username));
                if (empty($theUser)) {
                    $theUser = R::findOne($type, ' user_email = ? ', array($_email));
                }
            }

            if (isset($theUser) && !empty($theUser)) {

                $hash = $theUser->user_pass;
                $type = $theUser->type;

                if ($type) {

                    if (password_verify($_password, $hash)) {

                        /** Valid credentials, the user exists create the according Token */
                        $issuedAt = time();
                        $notBefore = $issuedAt;
                        $expire = $issuedAt + (86400 * 7); //todo centralizuj odavde i ubaci u filter editabilni

                        $token = array(
                            'iss' => 'http://example.org',
                            'iat' => $issuedAt,
                            'nbf' => $notBefore,
                            'exp' => $expire,
                            'data' => array(
                                'user' => array(
                                    'id' => $theUser->id,
                                    'type' => $theUser->type
                                ),
                            ),
                        );

                        /** Let the user modify the token data before the sign. */
                        //                $token = JWT::encode($token, $secret_key);
                        $token = $this->encode($token);

                        /** The token is signed, now create the object with no sensible user data to the client*/
                        $exitData = array(
                            'code' => $_code,
                            'token' => $token,
                            'user_email' => $theUser->user_email,
                            'type' => $type,
                        );

                        // Store User Token
                        $_token = R::dispense('tokens');
                        $_token->token = $token;
                        $_token->iss = 'http://example.org';
                        $_token->iat = $issuedAt;
                        $_token->nbf = $notBefore;
                        $_token->exp = $expire;

                        $theUser->xownTokenList[] = $_token;
                        try {
                            R::store($theUser);
                        } catch (SQL $e) { //todo
                        }

                        return $exitData;
                    } else {
                        $response['code'] = 1001;
                        $response['error'] = 'Wrong username or password';
                    }
                } else {
                    $response['code'] = 1000;
                    $response['error'] = 'User or type not found';
                }

            }

            return $response;
        }
        return null;
    }

// todo ... zavrsi





}