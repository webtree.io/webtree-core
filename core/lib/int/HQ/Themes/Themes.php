<?php


namespace WebTree\lib\int\HQ\Themes;


use GuzzleHttp\Client as client;

class Themes
{

    public static function getWTThemes() {
        $client = new client();
        $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/themes', []);
        $exit = json_decode($res->getBody());
        return isset($exit) ? $exit : null;
    }

    public static function installWTTheme($name) {
        $libName = $name;
        $libNameArr = explode(' ', $libName);

        $client = new client();
        $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/themes/'.$libNameArr[0], []);
        $exit = json_decode($res->getBody());

        $done = [];
        $errors = [];
        if (isset($exit) && !empty($exit)) {
            foreach ($exit as $item) {

                $url = "https://webtree.iridiumintel.com/storage/themes/$item";
                $pathArray = explode("/", $item);
                $pathCounter = 0;
                foreach ($pathArray as &$path) {
                    if ($path == basename($url)) {
                        unset($pathArray[$pathCounter]);
                    }
                    $pathCounter++;
                }
                $pathStr = implode("/", $pathArray);

                if (!file_exists(THEPATH."static/themes/$pathStr")){
                    mkdir(THEPATH."static/themes/$pathStr", 0755, true);
                }
                $file_name = THEPATH."static/themes/$pathStr/".basename($url);

                if(file_put_contents($file_name, @file_get_contents($url))) {
                    $done[] = $file_name;
                }
                else {
                    $errors['error'][] = $file_name;
                }

            }

            return $done+$errors;

        } else {
            return null;
        }
    }

}