<?php


namespace WebTree\lib\int\HQ\Libs;


use GuzzleHttp\Client as client;
use Siler\Http\Response;
use Siler\Twig;
use WebTree\lib\int\HQ\Themes\Themes;
use WebTree\ToolBox;
use WebTree\WebTree;


class Libs
{

    /**
     * Libs constructor.
     */
    public function __construct()
    {
    }

    public static function getWTLibs() {
        $client = new client();
        $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/libs', []);
        $exit = json_decode($res->getBody());
        return isset($exit) ? $exit : null;
    }



    public static function installWTLib($name) {
        $libName = $name;
        $libNameArr = explode(' ', $libName);

        if ($libNameArr[0] == 'all') {

            $client = new client();
            $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/libs', []);
            $exit = json_decode($res->getBody());
            if (isset($exit) && !empty($exit)) {
                foreach ($exit as $item) {

                    $resX = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/libs/'.$item, []);
                    $exitX = json_decode($resX->getBody());

                    if (isset($exitX) && !empty($exitX)) {
                        $iX = 1;
                        $xLenght = count($exitX);
                        foreach ($exitX as $itemX) {


                            $url = "https://webtree.iridiumintel.com/storage/lib/$itemX";

                            $pathArray = explode("/", $itemX);
                            $pathCounter = 0;
                            foreach ($pathArray as &$path) {
                                if ($path == basename($url)) {
                                    unset($pathArray[$pathCounter]);
                                }
                                $pathCounter++;
                            }
                            $pathStr = implode("/", $pathArray);

                            if (!file_exists(THEPATH."static/lib/$pathStr")){
                                mkdir(THEPATH."static/lib/$pathStr", 0755, true);
                            }
                            $file_name = THEPATH."static/lib/$pathStr/".basename($url);

                            if(file_put_contents($file_name, @file_get_contents($url))) {
//                                $output->writeln('<info>done</info>');
                            }
                            else {
                            }
                            $iX++;


                        }

                        return true;
                    }



                }



            } else {
                return null;
            }

        } else {
            $client = new client();
            $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/libs/'.$libNameArr[0], []);
            $exit = json_decode($res->getBody());
            if (isset($exit) && !empty($exit)) {
                $iX = 1;
                $xLenght = count($exit);
                foreach ($exit as $item) {


                    $url = "https://webtree.iridiumintel.com/storage/lib/$item";

                    $pathArray = explode("/", $item);
                    $pathCounter = 0;
                    foreach ($pathArray as &$path) {
                        if ($path == basename($url)) {
                            unset($pathArray[$pathCounter]);
                        }
                        $pathCounter++;
                    }
                    $pathStr = implode("/", $pathArray);

                    if (!file_exists(THEPATH."static/lib/$pathStr")){
                        mkdir(THEPATH."static/lib/$pathStr", 0755, true);
                    }
                    $file_name = THEPATH."static/lib/$pathStr/".basename($url);

                    if(file_put_contents($file_name, @file_get_contents($url))) {
                    }
                    else {
                    }
                    $iX++;


                }


                return true;

            } else {
                return null;
            }
        }
        return null;
    }



    public function isStaticLibInstalled($name) {
        $libsArr = self::getStaticLibNames();
        return in_array($name, $libsArr) ? true : false;
    }

    public function getStaticLibs() {
        $tools = new ToolBox();
        $libs = $tools->get_filelist_as_array(THEPATH.'static/lib'); //todo
        return isset($libs) ? $libs : null;
    }

    public function getStaticLibNames() {
        $libs = self::getStaticLibs();
        $exit = [];
        foreach ($libs as $lib) {
            $libArr = explode("/", $lib);
            if (!in_array($libArr[0], $exit)) {
                $exit[] = $libArr[0];
            }
        }
        return $exit;
    }


}