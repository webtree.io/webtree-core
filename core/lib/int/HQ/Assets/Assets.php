<?php


namespace WebTree\lib\int\HQ\Assets;


use GuzzleHttp\Client as client;
use WebTree\ToolBox;

class Assets
{

    public static function getWTAssets() {
        $client = new client();
        $res = $client->request('GET', 'https://webtree.iridiumintel.com/api/v1/assets', []);
        $exit = json_decode($res->getBody());
        return isset($exit) ? $exit : null;
    }


    public function isAssetInstalled($name) {
        $libsArr = self::getAssetNames();
        return in_array($name, $libsArr) ? true : false;
    }

    public function getAssets() {
        $tools = new ToolBox();
        $assets = $tools->get_filelist_as_array(THEPATH.'static/assets');
        return isset($assets) ? $assets : null;
    }

    public function getAssetTypes() {
        $assets = self::getAssets();
        $exit = [];
        foreach ($assets as $asset) {
            $assetArr = explode("/", $asset);
            if (!in_array($assetArr[0], $exit)) {
                $exit[] = $assetArr[0];
            }
        }
        return $exit;
    }

    public function getAssetNames() {
        $assets = self::getAssets();
        $exit = [];
        foreach ($assets as $asset) {
            $assetArr = explode("/", $asset);
            if (!in_array($assetArr[1], $exit)) {
                $exit[] = $assetArr[1];
            }
        }
        return $exit;
    }

    public function getAssetsByType($type) {
        $assets = self::getAssets();
        $exit = [];
        foreach ($assets as $asset) {
            $assetArr = explode("/", $asset);
            if ($assetArr[0] == $type) {
                $exit[] = $asset;
            }
        }
        return $exit;
    }
    public function getAssetsByName($name) {
        $assets = self::getAssets();
        $exit = [];
        foreach ($assets as $asset) {
            $assetArr = explode("/", $asset);
            if ($assetArr[1] == $name) {
                $exit[] = $asset;
            }
        }
        return $exit;
    }

}