<?php


namespace WebTree;


use WebTree\lib\int\Tangle\Helper;

class Validator
{

    protected $helper;

    /**
     * Validator constructor.
     */
    public function __construct() {
        $this->helper = new Helper();
    }


    public function userAuthObj(array $data) {
        $model = ['username', 'email', 'password'];
        if (!isset($data['email'])) {
            $data['email'] = ''; //todo....
        }
        return $this->helper->valByModel($model, $data);
    }
}