<?php


namespace WebTree\lib\int\Tangle;


class Helper
{

    /**
     * Helper constructor.
     */
    public function __construct()
    {
    }

    public function valByModel(array $model, array $data) {
        $exit = [];
        $target = count($model);
        $matched = 0;
        foreach ($model as $key) {
            foreach ($data as $dKey => $value) {
                if ($key == $dKey) {
                    $exit[$key] = strip_tags($value);
                    $matched++;
                }
            }
        }
        return $target == $matched ? $exit : null;
    }
}