<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/18/19
 * Time: 12:56 AM
 */

namespace WebTree;


use PDO;
use PDOException;
use RedBeanPHP\RedException\SQL;
use Siler\Monolog as Log;
use Siler\Http\Response;
use Siler\Http\Request;
use RedBeanPHP\Facade as R;
use \Firebase\JWT\JWT;
use WebTree\SecOps;
use WebTree\User as user;

class Installer
{

    private function valInitData(array $data) {
        if (isset($data) && !empty($data)) {
            if (isset($data['db_type'], $data['db_name'], $data['db_user'],
                $data['db_pwd'], $data['db_host'], $data['site_title'],
                $data['admin_user'], $data['admin_email'], $data['admin_pwd'])) {
                return true;
            }
        }
        return false;
    }

    private function rndGen ($length = 21) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*()_+.';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function WebTreeInit(array $data) {

        $dbData = array();
        $siteData = array();
        $adminData = array();

        $dbData['db_type'] = $data['db_type'];
        $dbData['db_name'] = $data['db_name'];
        $dbData['db_user'] = $data['db_user'];
        $dbData['db_pwd'] = $data['db_pwd'];
        $dbData['db_host'] = $data['db_host'];

        $default_salt = $this->rndGen();

        $adminData['admin_user'] = $data['admin_user'];
        $adminData['admin_email'] = $data['admin_email'];
        $adminData['admin_pwd'] = $data['admin_pwd'];


        if ($this->valInitData($data)) {


                $db_type = $data['db_type'];
                $db_name = $data['db_name'];
                isset($data['db_host']) ? $db_host = $data['db_host'] : $db_host = null;
                isset($data['db_user']) ? $db_user = $data['db_user'] : $db_user = null;
                isset($data['db_pwd']) ? $db_pwd = $data['db_pwd'] : $db_pwd = null;

                if (strtolower($db_type) == 'sqlite') { //todo
//                    $initDB = R::setup('sqlite:'.$db_name.'.db');
                    try{
                        $initDB = new PDO('sqlite:'.$db_name.'.db');
                    } catch(PDOException $e){
                        return $e->getmessage();
                    }
                } elseif (strtolower($db_type) == 'mysql' || strtolower($db_type) == 'mariadb') {
//                    $initDB = R::setup( 'mysql:host='.$db_host.';dbname='.$db_name,
//                        $db_user, $db_pwd ); //for both mysql or mariaDB
                    try{
                        $initDB = new PDO('mysql:host='.$db_host.';dbname='.$db_name,
                            $db_user, $db_pwd);
                    } catch(PDOException $e){
                        return $e->getmessage();
                    }
                } elseif (strtolower($db_type) == 'pgsql') {
//                    $initDB = R::setup( 'pgsql:host='.$db_host.';dbname='.$db_name,
//                        $db_user, $db_pwd );
                    try{
                        $initDB = new PDO('pgsql:host='.$db_host.';dbname='.$db_name,
                            $db_user, $db_pwd);
                    } catch(PDOException $e){
                        return $e->getmessage();
                    }
                } elseif (strtolower($db_type) == 'cubrid') {
//                    $initDB = R::setup('cubrid:host='.$db_host.';port=30000;dbname='.$db_name,
//                        $db_user, $db_pwd);
                    try{
                        $initDB = new PDO('cubrid:host='.$db_host.';port=30000;dbname='.$db_name,
                            $db_user, $db_pwd);
                    } catch(PDOException $e){
                        return $e->getmessage();
                    }
                }


                if (isset($initDB)) {

                    if (strtolower($db_type) == 'sqlite') { //todo
                        R::setup('sqlite:'.$db_name.'.db');
                    } elseif (strtolower($db_type) == 'mysql' || strtolower($db_type) == 'mariadb') {
                        R::setup( 'mysql:host='.$db_host.';dbname='.$db_name,
                            $db_user, $db_pwd ); //for both mysql or mariaDB
                    } elseif (strtolower($db_type) == 'pgsql') {
                        R::setup( 'pgsql:host='.$db_host.';dbname='.$db_name,
                            $db_user, $db_pwd );
                    } elseif (strtolower($db_type) == 'cubrid') {
                        R::setup('cubrid:host='.$db_host.';port=30000;dbname='.$db_name,
                            $db_user, $db_pwd);
                    }

                    $core_entry = R::dispense( 'core' );
                    $core_entry->installed = 1;
                    try {
                        $core_entry_id = R::store($core_entry);
                    } catch (SQL $e) {
                        //echo $e;
                    }

                    if (isset($core_entry_id)) {
                        if (copy('.ini.php', '.webtree.ini.php')) {

                            // todo title in ini? :O move to db
                            $this->update_config('.webtree.ini.php', 'site', 'title', $data['site_title']);

                            $this->update_config('.webtree.ini.php', 'database', 'type', $data['db_type']);
                            $this->update_config('.webtree.ini.php', 'database', 'name', $data['db_name']);
                            $this->update_config('.webtree.ini.php', 'database', 'user', $data['db_user']);
                            $this->update_config('.webtree.ini.php', 'database', 'pwd', $data['db_pwd']);
                            $this->update_config('.webtree.ini.php', 'database', 'host', $data['db_host']);

                            $this->update_config('.webtree.ini.php', 'variables', 'default_salt', $default_salt);

                            $admin = $this->addAdmin($adminData);
                            if (isset($admin) && !empty($admin)) {
                                Heimdall\redirect("/", true);
                            }

                        }
                    }
                }


        }
        return false;
    }

    protected function addAdmin(array $data) { //todo ??

        $_username = preg_replace("/[^a-zA-Z0-9\s]/", "", $data['admin_user']);
        $_email = $data['admin_email'];
        $_password = $data['admin_pwd'];

        $actKey = sha1(mt_rand(10000,99999).time().$_email);
        $_now = time();
        $pwdHash = password_hash($_password, PASSWORD_DEFAULT);

        $user = R::dispense( 'user' );

        $user->user_login = $_username;
        $user->user_pass = $pwdHash;
        $user->uid = $actKey;
        $user->user_nicename = $_username;
        $user->user_email = $_email;
        $user->type = 'user';
        $user->user_registered = $_now;
        $user->user_status = 0;
        $user->display_name = $_username;

        try {
            $user_id = R::store($user);
        } catch (SQL $e) {
            //echo $e;
        }


        return $user;
    }


    protected function initCore(array $data) {

        $core_entry = R::dispense( 'core' );
        $core_entry->active_theme = 'WebTreeSeed';
        $core_entry->version = '0.0.1'; // todo ... samo hardko(r)d

        try {
            $core_entry_id = R::store($core_entry);
        } catch (SQL $e) {
            //echo $e;
        }
    }

    // todo POC - not for use
    protected function update_config($config_file, $section, $key, $value) {
        $config_data = parse_ini_file($config_file, true);
        $config_data[$section][$key] = $value;
        $new_content = ";<?php
;echo 'Action noted';
;die();
;/*\n";
        foreach ($config_data as $section => $section_content) {
            $section_content = array_map(function($value, $key) {
                return "$key=\"$value\"";
            }, array_values($section_content), array_keys($section_content));
            $section_content = implode("\n", $section_content);
            $new_content .= "[$section]\n$section_content\n";
        }
        $new_content .= ";*/
;?>\n";
        file_put_contents($config_file, $new_content);
    }

}