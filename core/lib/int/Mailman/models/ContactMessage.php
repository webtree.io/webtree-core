<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 7/6/19
 * Time: 1:18 AM
 */

namespace WebTree\lib\int\Mailman\models;


class ContactMessage
{

    public $index;


    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->index = [
            'uid', 'user_id', 'title', 'description', 'type', 'active',
            'status', 'email', 'first_name', 'last_name', 'subject', 'message'
        ];


    }

    public function __get($var) {
        return null;
    }

    public function __set($var, $value) {
        $this->__get($var);
    }
}