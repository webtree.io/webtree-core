<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 7/19/19
 * Time: 8:43 PM
 */

namespace WebTree;


use RedBeanPHP\Facade as R;
use RedBeanPHP\RedException\SQL;
use WebTree\lib\int\Mailman\models\Newsletter as newsletterMod;
use WebTree\lib\int\Mailman\models\ContactMessage as contactMessageMod;
use function WebTree\Heimdall\redirect;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use WebTree\SecOps as secops;

class Mailman
{

    public static function sendSwift($to, $message, $subject = 'pong', $template = 'default') {

        $so = new secops();
        $configContainer = $so->get_config(THEPATH.'.webtree.ini.php');

        if (!isset($configContainer)) {
            echo 'Config file is missing. Installed?';
            exit(2);
        }

        $host = $configContainer['mailman']['host'];
        $port = $configContainer['mailman']['port'];
        $encryption = $configContainer['mailman']['encryption'];
        $username = $configContainer['mailman']['username'];
        $password = $configContainer['mailman']['password'];


        // Create the Transport
        $transport = (new Swift_SmtpTransport($host, $port,$encryption))
        //            ->setUsername('smarttelefon001@gmail.com')
            ->setUsername($username)
            ->setPassword($password)
        ;

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message = (new Swift_Message($subject))
            ->setFrom(['webtree@iridiumintel.com' => 'bad_ip'])
            ->setTo([$to])
            ->setBody($message)
            ->setContentType("text/html")
        ;

        // Send the message
        $result = $mailer->send($message);

        return $result;

    }


    public function newsletterAdd(array $data) {
        $response = [];
        if (isset($data) && isset($data['email'])) {

            $_email = strip_tags($data['email']);
            if (!self::newsletterGet($_email)) {
                isset($data['type']) ? $type = $data['type'] : $type = 'front';
                $_now_ts = time();
//                $mail = new mailman();
                $newsletterModel = new newsletterMod();
                $write = true;  //mislim... :robot:

                $newsletter = R::dispense( 'newsletter' );

                $newsletter->registered_ts = $_now_ts;
                $newsletter->type = $type;
                $newsletter->active = 0;

                foreach ($data as $index => $value) {
                    if (in_array($index, $newsletterModel->index)) {
                        $write = true;
                        $newsletter[$index] = $value;
                    }
                }

                try {
                    $write ? $newsletter_id = R::store($newsletter) : null;
                    $response['code'] = 777;
                    $response['id'] = $newsletter_id;
                } catch (SQL $e) {
                    //echo $e; //todo error handling
                }
            } else {
                $response['code'] = 666;
            }

//                $mail->sendSwift($_email, 'Welcome to Web<b>Tree</b> '.$_username, 'Welcome');

            return $response;

        }

        return $response;
    }

    public function newsletterGet($email) {
        $theOne = R::findOne('newsletter',
            ' email = ? ',array($email));
        return isset($theOne) ? $theOne : null;
    }

    public function newsletterUpdate($email, array $data) {


        if (isset($email) && !empty($data)) {
            $updated = false;
            $tools = new ToolBox();
            if ($tools->hasSeed('newsletter')) {
                $isHereUser = R::findOne('newsletter',
                    ' email = ? ',array($email));
            }

            if (!empty($isHereUser)) {
                $newsletterMod = new newsletterMod();
                foreach ($data as $index => $value) {
                    if (in_array($index, $newsletterMod->index)) {
                        $updated = true;
                        $isHereUser[$index] = $value;
                    }
                }

                try {
                    $updated ? $newsletter_id = R::store($isHereUser) : null;
                } catch (SQL $e) {
                    //echo $e;
                }

            }
        }

    }

    public function newsletterGetAll($page = 1, $limit = 10) {
        if ($limit == 'all') {
            $all = R::findAll('newsletter',' ORDER BY id DESC ', []);
        } else {
            $all = R::findAll('newsletter',' ORDER BY id DESC LIMIT '.(($page-1)*$limit).', '.$limit, []);
        }
        return !empty($all) ? $all : null;
    }


    public function contactMessage(array $data) { //todo nejming? :>
        $response = [];
        if (isset($data, $data['first_name'], $data['last_name'], $data['subject'], $data['email'], $data['message'])) {

            isset($data['type']) ? $type = $data['type'] : $type = 'front';
            $_now_ts = time();
//                $mail = new mailman();
            $contactMessageModel = new contactMessageMod();
            $write = true;  //mislim... :robot:

            $contactMessage = R::dispense( 'contactmessage' );

            $contactMessage->created_ts = $_now_ts;
            $contactMessage->type = $type;
            $contactMessage->active = 1;

            foreach ($data as $index => $value) {
                if (in_array($index, $contactMessageModel->index)) {
                    $write = true;
                    $contactMessage[$index] = strip_tags($value);
                }
            }

            try {
                $write ? $contactMessage_id = R::store($contactMessage) : null;
                $response['code'] = 777;
                $response['id'] = $contactMessage_id;
            } catch (SQL $e) {
                //echo $e; //todo error handling
            }
//                $mail->sendSwift($_email, 'Welcome to Web<b>Tree</b> '.$_username, 'Welcome');

            return $response;

        }

        return $response;
    }
}