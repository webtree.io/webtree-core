<?php

namespace WebTree; //todo comply with psr

use WebTree\lib\int\Data\Constants;
//use WebTree\ImageHelper;
//use WebTree\Admin;
//use WebTree\Integrations;
//use WebTree\PostGetter;
//use WebTree\TermGetter;
//use WebTree\Site;
//use WebTree\URLHelper;
//use WebTree\Helper;
//use WebTree\Pagination;
use WebTree\User;
//use WebTree\Loader;
use Siler\Http\Request as request;
use Siler\Route;
use Siler\Http\Response;
use Siler\Twig;

/**
 * WebTree Class.
 *
 * Main class called WebTree
 */
class WebTree
{

    public static $version = '0.0.1';
    public static $locations;
    public static $dirname = 'views';
    public static $twig_cache = false;
    public static $cache = false;
    public static $auto_meta = true;
    public static $autoescape = false;

    public static $context_cache = array();


    /**
     * @codeCoverageIgnore
     */
    public function __construct()
    {
        if (!defined('THEPATH')) {
            return;
        }
        self::init();
    }

    public static function context()
    {
        return self::get_context();
    }

    /**
     * Get context.
     * @api
     * @return array
     */
    public static function get_context() {
        if (empty(self::$context_cache)) {
            $tangle = new Tangle();
            $user = new User();
            $tools = new ToolBox();

            self::$context_cache['thepath'] = THEPATH;
            self::$context_cache['request'] = request\get();
            self::$context_cache['request']['raw'] = Request\raw();
            self::$context_cache['request']['params'] = Request\params();
            self::$context_cache['request']['post'] = Request\post();
            self::$context_cache['request']['get'] = Request\get();
            self::$context_cache['request']['json'] = Request\json();
            self::$context_cache['currentUser'] = ($user->currentUser) ? $user->currentUser : false;
            self::$context_cache['userProfileImgSrc'] = ($user->currentUser) ? $tools->getUserProfileImage($user->currentUser->uid) : '/static/assets/images/users/no-photo.png';

            $strings = new \LitStrings\LitStrings(THEPATH);
            self::$context_cache['strings'] = $strings->strings;


//            if ($lng = Request\get('lng')) { //todo modable during installation
//                $strings = new \LitStrings\LitStrings(THEPATH, $lng);
//                self::$context_cache['strings'] = $strings->strings;
//            } else {
//                $strings = new \LitStrings\LitStrings(THEPATH);
//                self::$context_cache['strings'] = $strings->strings;
//            }

            self::$context_cache = $tangle->hooks->apply_filters('webtree_context', self::$context_cache);
            self::$context_cache = $tangle->hooks->apply_filters('webtree/context', self::$context_cache);
        }

        return self::$context_cache;
    }

    /**
     * @codeCoverageIgnore
     */
    protected static function init() {
        if (!defined('WEBTREE_LOADED')) {
            Constants::init_constants();
            define('WEBTREE_LOADED', true);
        }
    }

    /**
     * @codeCoverageIgnore
     * @param string $dir
     */
    public static function initFileRouter($dir = THEPATH.'core/controllers') {
        $dirs = ToolBox::getSubDirectories($dir, false);
        if (isset($dirs) && !empty($dirs)) {
            unset($dirs[0]); //todo
            foreach ($dirs as $dir) {
                Route\files($dir);
            }
        }
    }

    public function killRegex($regex) {

        $exec = "ps aux|grep -E '$regex' | grep -v grep | awk '{print $2}'";
        exec($exec, $output);

        if (count($output) > 0) {
            $exec = "kill " . $output[0];
            $this->executeIt($exec);
            return $output[0];
        }
        return false;
    }

    public function psRegex($regex)
    {

        $exec = "ps aux|grep -E '$regex' | grep -v grep | awk '{print $2}'";
        exec($exec, $output);

        if (count($output) > 0) {
            return $output[0];
        }
        return false;
    }

    function executeIt($exec, $mode = 'sudo') {

        $exec_mode = $mode;
        if ($exec_mode == "danger") {
            $bin_exec = "/usr/share/blackbulb/bin/danger";
            exec("$bin_exec \"" . $exec . "\"", $output);

            return $output;
        } elseif ($exec_mode == "sudo") {
            $bin_exec = "/usr/bin/sudo";
            exec("$bin_exec sh -c \"$exec\"", $output);

            return $output;
        } else {
            $bin_exec = "/usr/bin";
            exec("$bin_exec sh -c \"$exec\"", $output);
            return $output;
        }
//        return false;
    }


    function recursiveDelete($str) {
        if (is_file($str)) {
            return @unlink($str);
        } elseif (is_dir($str)) {
            $scan = glob(rtrim($str, '/').'/*');
            foreach ($scan as $index => $path) {
                self::recursiveDelete($path);
            }
            return @rmdir($str);
        }
        return false;
    }

    public function clearCache()
    {
        $files = glob('cache/*'); // get all file names
        self::recursiveDelete('cache/');
        return true; //todo
    }


    public static function renderView($view, $context, $options = []) { //todo testing
        try {
            $html = Twig\render($view, $context);
        } catch (\Twig\Error\LoaderError $e) {
        } catch (\Twig\Error\RuntimeError $e) {
        } catch (\Twig\Error\SyntaxError $e) {
        }
        return Response\html($html);
    }
}
