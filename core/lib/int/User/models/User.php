<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 7/6/19
 * Time: 1:18 AM
 */

namespace WebTree\lib\int\User\models;


class User
{

    public $index;
    private $protected;


    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->index = [
            'image_id', 'first_name', 'last_name', 'user_login', 'user_pass', 'user_nicename', 'user_email', 'user_registered',
            'user_activation_key', 'user_status', 'type', 'display_name', 'active', 'telephone', 'mobile_phone',
            'billing_address', 'post_code', 'state', 'address1', 'address2', 'city', 'newsletter', 'password_reset_code',
            'preferred_billing_currency', 'language', 'country_id', 'last_login', 'gender', 'birth_date', 'measurement', 'student_id',
            'preferred_unit', 'deleted', 'deleted_ts', 'auto_renewal', 'mobile_platform', 'affiliate_id', 'profile_picture',
            'user_slug', 'activity_level', 'userfcm', 'oauth_provider', 'oauth_uid', 'picture', 'locale', 'link'
        ];

        $this->protected = [
            'user_pass', 'gender'
        ];


    }

    public function __get($var) {
        return null;
    }

    public function __set($var, $value) {
        $this->__get($var);
    }
}