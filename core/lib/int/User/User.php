<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/19/19
 * Time: 11:45 PM
 */

namespace WebTree;

use RedBeanPHP\Facade as R;
use RedBeanPHP\RedException\SQL;
use WebTree\Mailman as mailman;
use WebTree\lib\int\User\models\User as userMod;

class User
{

    public $currentUser;
    protected $validator;
    protected $tools;

    /**
     * User constructor.
     */
    public function __construct()
    {
        @$this->currentUser = self::currentUser();
        $this->validator = new Validator();
        $this->tools = new ToolBox();

    }


    /**
     * @param string|array $data
     * @return NULL|\RedBeanPHP\OODBBean
     */
    public function userExists($data) {
        if ($this->tools->hasSeed('user')) {
            if (is_array($data)) {
                foreach ($data as $value) {
                    $isHereUser = R::findOne('user',
                        ' user_email = ? ',array($value));
                    if (empty($isHereUser)) {
                        $isHereUser = R::findOne('user',
                            ' user_login = ? ',array($value));
                    }
                    if (empty($isHereUser)) {
                        $isHereUser = R::findOne('user',
                            ' phone_number = ? ',array($data));
                    }
                }
                return !empty($isHereUser) ? $isHereUser : null;
            } else {
                $isHereUser = R::findOne('user',
                    ' user_email = ? ',array($data));
                if (empty($isHereUser)) {
                    $isHereUser = R::findOne('user',
                        ' user_login = ? ',array($data));
                }
                if (empty($isHereUser)) {
                    $isHereUser = R::findOne('user',
                        ' phone_number = ? ',array($data));
                }
                return !empty($isHereUser) ? $isHereUser : null;
            }
        }
        return null;
    }

    public function regAdmin($dataRe) {
        $response = null;
        $data = $this->validator->userAuthObj($dataRe);
        if (isset($dataRe['type']) && $data) {

            $_type = $dataRe['type'];

            $_username = $data['username'];
            $_password = $data['password'];
            $_email = $data['email'];
            $response = array();

            $isHereUser = null;
            $isHereEmail = null;

            if ($this->tools->hasSeed('user')) {
                $isHereUser = R::findOne('user',
                    ' user_login = ? ',array($_username));
                $isHereEmail = R::findOne('user',
                    ' user_email = ? ',array($_email));
            }
            if (!isset($isHereUser)) {
                $response['error'] = 'Username already registered;';
            }
            if (!isset($isHereEmail)) {
                $response['error'] = 'Email already registered;';
            }


            if (is_null($this->userExists([$_username, $_email]))) {
                $actKey = sha1(mt_rand(10000,99999).time().$_email);
                $_now = date("Y-m-d H:i:s");
                $pwdHash = password_hash($_password, PASSWORD_DEFAULT);

                $user = R::dispense( 'user' );

                $user->user_login = $_username;
                $user->user_pass = $pwdHash;
                $user->uid = $actKey;
                $user->user_nicename = $_username;
                $user->user_email = $_email;
                $user->user_registered = time();
                $user->user_status = 0;
                $user->type = $_type;
                $user->display_name = $_username;
                // todo role details

                try {
                    $user_id = R::store($user);
                } catch (SQL $e) { //todo
                }

                $response['user'] = $user;

            }

        }
        return $response;

    }

    public function addUser(array $data) {
        $response = [];
        $data = $this->validator->userAuthObj($data);

        if ($data) {

            $_username = $data['username'];
            $_password = $data['password'];
            $_email = $data['email'];
            isset($data['type']) ? $type = $data['type'] : $type = 'user';

            if (!self::userExists([$_username, $_email])) {

                $actKey = sha1(mt_rand(10000,99999).time().$_username);
                $_now = date("Y-m-d H:i:s");
                $_now_ts = time();
                $pwdHash = password_hash($_password, PASSWORD_DEFAULT);

//                $mail = new mailman();
                $userModel = new userMod();
                $write = true;  //mislim... :robot:

                $user = R::dispense( 'user' );

                $user->user_login = $_username;
                $user->user_pass = $pwdHash;
                $user->uid = $actKey;
                $user->user_email = $_email;
                $user->user_registered = $_now_ts;
                $user->display_name = $_username;
                $user->type = $type;
                $user->active = 0;

                if ($_username) {
                    $isHereUserSlug = R::findOne('user',
                        ' user_slug = ? ',array(strtolower($_username)));
                    if (empty($isHereUserSlug)) {
                        $user->user_slug = strtolower($_username);
                    } else {
                        $user->user_slug = strtolower($_username) . rand(1, 1024);
                    }
                }

                foreach ($data as $index => $value) {
                    if (in_array($index, $userModel->index)) {
                        $write = true;
                        $user[$index] = $value;
                    }
                }

                try {
                    $write ? $user_id = R::store($user) : null;
                    $response['code'] = 777;
                    $response['uid'] = $user_id;
                } catch (SQL $e) {
                    //echo $e; //todo error handling
                }

//                $mail->sendSwift($_email, 'Welcome to Web<b>Tree</b> '.$_username, 'Welcome');

                return $response;

            } else {
                $response['code'] = 666;
            }

        }

        return $response;
    }

    public function updateUser(int $id, array $data) {


        if (isset($id) && !empty($data)) {
            $updated = false;

            if ($this->tools->hasSeed('user')) {
                $isHereUser = R::load( 'user', $id );
            }

            if (!empty($isHereUser)) {

                $_now = date("Y-m-d H:i:s");
                $userMod = new userMod();

                foreach ($data as $index => $value) {

                    if (in_array($index, $userMod->index)) {
                        $updated = true;

                        if ($index == 'userfcm') {
                            if (!empty($isHereUser->userfcm_id)) {
                                $isHereUserFcm = R::load( 'userfcm', $isHereUser->userfcm_id );
                                $isHereUserFcm->token = $value;
                                $isHereUserFcm->input_ts = time();
                                $isHereUserFcm->user_id = $isHereUser->id;
                                try {
                                    R::store($isHereUserFcm);
                                } catch (SQL $e) {
                                    //echo $e;
                                }
                            } else {
                                $_token = R::dispense( 'userfcm' );
                                $_token->token = $value;
                                $_token->input_ts = time();
                                $_token->user_id = $isHereUser->id;
                                $isHereUser->userfcm = $_token;
                            }
                            continue;
                        }

                        //handling if fields are not set unique
                        if ($index == 'user_email' || $index == 'email') {
                            $isHereEmail = self::userExists($value);
                            if (isset($isHereEmail) && !empty($isHereEmail)) {
                                continue;
                            }
                        }
                        if ($index == 'phone_number' || $index == 'phone') {
                            $isHereEmail = self::userExists($value);
                            if (isset($isHereEmail) && !empty($isHereEmail)) {
                                continue;
                            }
                        }

                        $isHereUser[$index] = $value;
                    }

                }

                try {
                    $updated ? $user_id = R::store($isHereUser) : null;
                } catch (SQL $e) {
                    //echo $e;
                }

            }
        }

    }


    /**
     * @return NULL|\RedBeanPHP\OODBBean
     */
    private function currentUser() { //todo handle types? diff tables
        $lightUser = heimdall\get_user();
        isset($lightUser, $lightUser['id']) ? $currentUserID = $lightUser['id'] : $currentUserID = null;

        if ($currentUserID) {
            $user  = R::findOne( 'user', ' id = ? ', [ $currentUserID ] );
            return isset($user) ? $user : null;
        }
        return null;
    }


    public function getUser($id) {
        $user  = R::findOne( 'user', ' id = ? ', [ $id ] );
        !isset($user) ? $user  = R::findOne( 'user', ' uid = ? ', [ $id ] ) : null;
        return isset($user) ? $user : null;
    }

    public function getUserByUid($uid) {
        $user  = R::findOne( 'user', ' uid = ? ', [ $uid ] );
        return isset($user) ? $user : null;
    }

}