<?php


namespace WebTree\lib\int\User;


use RedBeanPHP\Facade as R;
use RedBeanPHP\RedException\SQL;
use WebTree\Formatting;
use WebTree\lib\int\User\models\Group as groupMod;
use WebTree\ToolBox;
use WebTree\User;

class Group
{

    public function groupExists($title) {
        $tools = new ToolBox();
        if ($tools->hasSeed('group')) {
            $isHere = R::findOne('group',
                ' title = ? ',array($title));
            return !empty($isHere) ? $isHere : null;
        }
        return null;
    }

    public function addGroup(array $data) {
        $response = [];

        if (isset($data['title'], $data['user_id'])) {

            $_title = $data['title'];
            $_uid = $data['user_id'];
            $format = new Formatting();

            if (!self::groupExists($_title)) {

                $_now_ts = time();
                $groupModel = new groupMod();
                $write = true;  //mislim... :robot:
                $actKey = sha1(mt_rand(10000,99999).time());

                $group = R::dispense( 'group' );

                $group->title = $_title;
                $group->slug = $format->sanitize_title_with_dashes($_title);
                $group->user_id = $_uid;
                $group->uid = $actKey;
                $group->created_ts = $_now_ts;
                $group->type = 'default';
                $group->active = 0;

                foreach ($data as $index => $value) {
                    if (in_array($index, $groupModel->index)) {
                        $write = true;
                        $group[$index] = $value;
                    }
                }

                try {
                    $write ? $group_id = R::store($group) : null;
                    if ($group_id) {
                        $response['code'] = 777;
                        $response['tid'] = $group_id;
                        //adduser to group
                        self::addUserToGroup($_uid, $group_id);
                    }

                } catch (SQL $e) {
                    //echo $e; //todo error handling
                }

                return $response;

            } else {
                $response['code'] = 666;
            }

        }

        return $response;
    }

    public static function getGroups($page = 1, $limit = 10) {
        if ($limit == 'all') {
            $all = R::findAll('group',' ORDER BY id DESC ', []);
        } else {
            $all = R::findAll('group',' ORDER BY id DESC LIMIT '.(($page-1)*$limit).', '.$limit, []);
        }
        return !empty($all) ? $all : null;
    }

    public function addUserToGroup($user, $group) {
        $userObj = new User();
        $theUser = $userObj->getUser($user);
        $userCurrentGroups = isset($theUser->groups) && !empty($theUser->groups) ? explode(",", $theUser->groups) : [];
        if (!in_array($group, $userCurrentGroups)) {
            $userCurrentGroups[] = $group;
        }
        if (count($userCurrentGroups) > 1) {
            $theUser->groups = implode(",", $userCurrentGroups);
        } else {
            $theUser->groups = $userCurrentGroups[0];
        }
        try {
            R::store($theUser);
        } catch (SQL $e) {
        }
    }

    public static function getAllGroupUsers($gid) {
        $beans = R::find('user',' teams LIKE :team ',
            array(':team' => '%' . $gid . '%' )
        );
//        $beans =  R::findLike( 'user', [
//            'groups' => [$gid]
//        ]);
        $exit = [];
        foreach ($beans as $bean) {
            $exit[] = [
                'id' => $bean->id,
                'display_name' => $bean->display_name,
            ];
        }
        return isset($exit) && !empty($exit) ? $exit : null;
    }

    public function removeUserFromGroup($user, $group) {
        $userObj = new User();
        $theUser = $userObj->getUser($user);
        $userCurrentGroups = isset($theUser->groups) && !empty($theUser->groups) ? explode(",", $theUser->groups) : [];
        $i = 0;
        foreach ($userCurrentGroups as $currentGroup) {
            if ($currentGroup == $group) {
                unset($userCurrentGroups[$i]);
            }
        }
        $theUser->groups = implode(",", $userCurrentGroups);
        try {
            R::store($theUser);
        } catch (SQL $e) {
        }
    }
}