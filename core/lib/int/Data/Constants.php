<?php


namespace WebTree\lib\int\Data;


class Constants
{


    /**
     * Constants constructor.
     */
    public function __construct()
    {
    }

    public static function init_constants() {
        defined("WT_LOC") or define("WT_LOC", realpath(dirname(__DIR__))); //todo
        defined("WT_URL") or define("WT_URL", "https://webtree.iridiumintel.com/");
        defined("WT_DEV_URL") or define("WT_DEV_URL", "http://0.0.0.0:44400/");
    }
}