<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 8/11/19
 * Time: 3:52 PM
 */

namespace WebTree;

use RedBeanPHP\Facade as R;

class FCM
{

//sendNotification("New post!", "How to send a simple FCM notification in php", ["new_post_id" => "605"], "YOUR_SERVER_KEY");


    public function getUsersFCM($user) {
        if (isset($user) && !empty($user)) {
            $theUser  = R::load( 'user', (int) $user);
            if (!empty($theUser)) {
                $userFcm = R::load( 'userfcm', $theUser->userfcm_id );
                if (!empty($userFcm)) {
//                    var_dump($userFcm->id);
                    return $userFcm;
                }
            }
        }
        return null;
    }


    public function sendNotification($to = "", $title = "", $body = "", $customData = []){
        $serverKey = 'fcm-server-key';
        if($serverKey != ""){
            ini_set("allow_url_fopen", "On");
            if (empty($to)) {
                $to = '/topics/global';
            }
            $data =
                [
                    "to" => $to,
                    "notification" => [
                        "body" => $body,
                        "title" => $title,
                    ],
                    "data" => $customData
                ];

            $options = array(
                'http' => array(
                    'method'  => 'POST',
                    'content' => json_encode( $data ),
                    'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n" .
                        "Authorization:key=".$serverKey
                )
            );

            $context  = stream_context_create( $options );
            $result = file_get_contents( "https://fcm.googleapis.com/fcm/send", false, $context );
            return json_decode( $result );
        }
        return false;
    }


    public function MultiandroidNotification($deviceToken, $message,$type,$title=null,$sub_title=null,$device_type=null,$data = null,$content_available = null) {

        if($content_available == 1){
            $content_available = false;
        }else{
            $content_available =  true;
        }
        if($type == 12 || $type == 13){
            $priority = '';
        }else{
            $priority = 'high';
        }

        $deviceToken = array_values($deviceToken);
        $no = null;

        $apiKey = 'XXXXXXXXXXXXXXXXXXXXXX';

        $notification = array("text" => "test",'badge' => "1",'sound' => 'shutter.wav','body'=>$message,'icon'=>'notif_icn','title'=>$title,'priority'=>'high','tag'=>'test','vibrate'=> 1,'alert'=> $message);

        $msg = array('message'=> $message,'title'=> $title,'sub_title'=> $sub_title,'type'=>$type,'activitydata' => $data);

        if($device_type == 'android'){
            $fields = array("content_available" => true,"priority" => "high",'registration_ids'=> $deviceToken,'data'=> $msg);
        }else{
            $fields = array('notification' => $notification,"content_available" => $content_available,"priority" => $priority,'registration_ids'=> $deviceToken,'data'=> $msg);
        }

        $headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');

        if ($headers){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $response = curl_exec($ch);
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            return false; //probably you want to return false
        }
        if ($httpCode != 200) {
            return false; //probably you want to return false
        }
        curl_close($ch);
        return $response;
    }

}