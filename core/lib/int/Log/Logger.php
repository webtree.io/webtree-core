<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 7/17/19
 * Time: 10:06 PM
 */

namespace WebTree\Log;

use Siler\Monolog as Log;

class Logger
{

    private $logType;

    /**
     * Logger constructor.
     * @param $logType
     */
    public function __construct($logType = 'general')
    {
        if (!file_exists(THEPATH.'/log/'.$logType)) {
            mkdir(THEPATH.'/log/'.$logType, 0777, true);
        }
        $logDir = THEPATH.'/log/'.$logType.'/';

        $this->logType = $logType;
        try {
            Log\handler(Log\stream($logDir . $this->logType . '-' . date('d-m-Y') . '.log'));
        } catch (\Exception $e) {
            // todo _\m/
        }
    }


    /**
     * @param string $message
     * @param array $data
     */
    public function note($message = 'pong', $data = []) {
        Log\debug($message, $data);
    }



}