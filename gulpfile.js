const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const del = require('del');
const minify = require('gulp-minify');
const cleanCss = require('gulp-clean-css');

gulp.task('pack-js', function () {
    return gulp.src(['static/assets/js/**/*.js'])
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('static/public/js'));
});

gulp.task('pack-styles', () => {
    return gulp.src('static/assets/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('static/public/css'));
});

gulp.task('clean', () => {
    return del([
        'static/public/css/main.css',
        'static/public/js/bundle.js',
    ]);
});

gulp.task('default', gulp.series(['clean', 'pack-styles', 'pack-js']));