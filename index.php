<?php
/**
 * Created by PhpStorm.
 * User: teodor
 * Date: 5/2/19
 * Time: 10:55 PM
 */
/** Define THEPATH as this file's directory */
if ( ! defined( 'THEPATH' ) ) {
    define( 'THEPATH', dirname( __FILE__ ) . '/' );
}
$composer_autoload = __DIR__.'/core/lib/vendors/autoload.php';
if ( file_exists( $composer_autoload ) ) {
    require_once $composer_autoload;
    $webTree = new \WebTree\WebTree();
}

if ( ! class_exists( '\WebTree\WebTree' ) ) {
    echo '<div class="error"><p>WebTree not installed</p></div>';
    die(0);
}

use Siler\Monolog as Log;
use Siler\Route;
use RedBeanPHP\Facade as R;
use Siler\Http\Response;
use Siler\Http\Request;
use Siler\Twig;
use WebTree\Heimdall as heimdall;
use WebTree\SecOps as secops;

heimdall\guard('index');

$_nonce = Request\get('nonce', null); //todo
$_action = Request\get('action', null);
if ($_action == 'logout' && $_nonce) {
    heimdall\log_out();
}

$so = new secops();
$configContainer = $so->get_config(THEPATH.'.webtree.ini.php');

if (!isset($configContainer)) {
    echo 'Config file is missing. Installed?';
    exit(2);
}

// init DB
if (isset($configContainer, $configContainer['database'],
    $configContainer['database']['type'], $configContainer['database']['name'])) {

    $db_type = $configContainer['database']['type'];
    $db_name = $configContainer['database']['name'];
    isset($configContainer['database']['host']) ? $db_host = $configContainer['database']['host'] : $db_host = null;
    isset($configContainer['database']['user']) ? $db_user = $configContainer['database']['user'] : $db_user = null;
    isset($configContainer['database']['pwd']) ? $db_pwd = $configContainer['database']['pwd'] : $db_pwd = null;

    if (strtolower($db_type) == 'sqlite') {
        R::setup('sqlite:'.$db_name.'.db');
    } elseif (strtolower($db_type) == 'mysql' || strtolower($db_type) == 'mariadb') {
        R::setup( 'mysql:host='.$db_host.';dbname='.$db_name,
            $db_user, $db_pwd ); //for both mysql or mariaDB
    } elseif (strtolower($db_type) == 'pgsql') {
        R::setup( 'pgsql:host='.$db_host.';dbname='.$db_name,
            $db_user, $db_pwd );
    } elseif (strtolower($db_type) == 'cubrid') {
        R::setup('cubrid:host='.$db_host.';port=30000;
    dbname='.$db_name,
            $db_user,$db_pwd);
    }
}


//$language = "en_US.UTF-8"; //todo https://litstrings.info

$shouldTwigDebug = true;
Twig\init('core/views/', 'cache', $shouldTwigDebug);


 /**
 * Route Controllers
 */
\WebTree\WebTree::initFileRouter();  //todo degrading speed? no+?

/**
 * Logs handler
 */
try {
    Log\handler(Log\stream(__DIR__ . '/WebTree_core_log'));
} catch (Exception $e) {
}


if (!Route\did_match()) {
    try {
        $html = Twig\render('404.twig');
        Response\html($html, 404);
    } catch (\Twig\Error\LoaderError $e) {
    } catch (\Twig\Error\RuntimeError $e) {
    } catch (\Twig\Error\SyntaxError $e) {
    }
}

$badIP = new \bad_ip\bad_ip();
$badIP->settings['log'] = false;
$badIP->init();