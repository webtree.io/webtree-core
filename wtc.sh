#!/bin/bash

echo "
               ,@@@@@@@,
       ,,,.   ,@@@@@@/@@,  .oo8888o.
    ,&%%&%&&%,@@@@@/@@@@@@,8888\88/8o
   ,%&\%&&%&&%,@@@\@@@/@@@88\88888/88'
   %&&%&%&/%&&%@@\@@/ /@@@88888\88888'
   %&&%/ %&%%&&@@\ V /@@' \`88\8 \`/88'
   \`&%\ \` /%&'    |.|        \ '|8'
       |o|        | |         | |
       |.|        | |         | |
    \\/ ._\//_/__/  ,\_//__\\/.  \_//__/_
            WebTree Composer
"
#echo "Project name? (no spaces)"
#read varname
#if [ "$varname" = "" ]; then
#    echo "Required"
#    exit 1
#fi
#name=$varname
#declare -l name
#name=$name;


echo "Web server port?"
read varname
if [ "$varname" = "" ]; then
    echo "Required"
    exit 1
fi
webPort=$varname
declare -l webPort
webPort=$webPort; #echo "$webPort"


echo "Database server port?"
read varname
if [ "$varname" = "" ]; then
    echo "Required"
    exit 1
fi
dbPort=$varname
declare -l dbPort
dbPort=$dbPort; #echo "$dbPort"

echo "Database root password"
read varname
if [ "$varname" = "" ]; then
    echo "Required"
    exit 1
fi
pwd=$varname


#mkdir "$name" && cd "$name" || exit #//todo

touch docker-compose.yml

cat > docker-compose.yml <<EOL
version: "2"
services:
  ${name}-db:
    image: mariadb
    ports:
      - "${dbPort}:3306"
    environment:
      - MYSQL_ROOT_PASSWORD=${pwd}
      - MYSQL_USER=public
      - MYSQL_PASSWORD=!webTree
      - MYSQL_DATABASE=webTreeApp
      # MYSQL_ROOT_HOST=%
      # MYSQL_ROOT_PASSWORD: ${pwd}
  ${name}-web:
    image: phpearth/php:7.4-apache
    volumes:
      - ./:/var/www/html
    ports:
      - "${webPort}:80"
    links:
      - ${name}-db:mysql
    environment:
      - DB_PASSWORD=${pwd}
EOL

sudo docker-compose up -d